/**
 * @file
 * Javascript behaviors for the Imotilux module.
 */

(function($, Drupal) {
  /**
   * Adds summaries to the imotilux outline form.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches summary behavior to imotilux outline forms.
   */
  Drupal.behaviors.imotiluxDetailsSummaries = {
    attach(context) {
      $(context)
        .find('.imotilux-outline-form')
        .drupalSetSummary(context => {
          const $select = $(context).find('.imotilux-title-select');
          const val = $select.val();

          if (val === '0') {
            return Drupal.t('Not in imotilux');
          }
          if (val === 'new') {
            return Drupal.t('New imotilux');
          }

          return Drupal.checkPlain($select.find(':selected').text());
        });
    },
  };
})(jQuery, Drupal);
