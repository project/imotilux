<?php

/**
 * @file
 * Provide views data for imotilux.module.
 *
 * @ingroup views_module_handlers
 */

/**
 * Implements hook_views_data().
 */
function imotilux_views_data() {
  $data = [];
  $data['imotilux'] = [];
  $data['imotilux']['table'] = [];
  $data['imotilux']['table']['group'] = t('Imotilux');

  $data['imotilux']['table']['join'] = [
    'node_field_data' => [
      'left_field' => 'nid',
      'field' => 'nid',
    ],
  ];

  $data['imotilux']['nid'] = [
    'title' => t('Page'),
    'help' => t('The imotilux page node.'),
    'relationship' => [
      'base' => 'node_field_data',
      'id' => 'standard',
      'label' => t('Imotilux Page'),
    ],
  ];

  $data['imotilux']['bid'] = [
    'title' => t('Top level imotilux'),
    'help' => t('The imotilux the node is in.'),
    'relationship' => [
      'base' => 'node_field_data',
      'id' => 'standard',
      'label' => t('Imotilux'),
    ],
  ];

  $data['imotilux']['pid'] = [
    'title' => t('Parent'),
    'help' => t('The parent imotilux node.'),
    'relationship' => [
      'base' => 'node_field_data',
      'id' => 'standard',
      'label' => t('Imotilux Parent'),
    ],
  ];

  $data['imotilux']['has_children'] = [
    'title' => t('Page has Children'),
    'help' => t('Flag indicating whether this imotilux page has children'),
    'field' => [
      'id' => 'boolean',
    ],
    'sort' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'boolean',
      'label' => t('Has Children'),
    ],
    'argument' => [
      'id' => 'numeric',
    ],
  ];

  $data['imotilux']['weight'] = [
    'title' => t('Weight'),
    'help' => t('The weight of the imotilux page.'),
    'field' => [
      'id' => 'numeric',
    ],
    'sort' => [
      'id' => 'standard',
    ],
  ];

  $data['imotilux']['depth'] = [
    'title' => t('Depth'),
    'help' => t('The depth of the imotilux page in the hierarchy; top level imotilux have a depth of 1.'),
    'field' => [
      'id' => 'numeric',
    ],
    'sort' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'numeric',
    ],
    'argument' => [
      'id' => 'standard',
    ],
  ];
  $parents = [
    1 => t('1st parent'),
    2 => t('2nd parent'),
    3 => t('3rd parent'),
    4 => t('4th parent'),
    5 => t('5th parent'),
    6 => t('6th parent'),
    7 => t('7th parent'),
    8 => t('8th parent'),
    9 => t('9th parent'),
  ];
  foreach ($parents as $i => $parent_label) {
    $data['imotilux']["p$i"] = [
      'title' => $parent_label,
      'help' => t('The @parent of imotilux node.', ['@parent' => $parent_label]),
      'relationship' => [
        'base' => 'node_field_data',
        'id' => 'standard',
        'label' => t('Imotilux @parent', ['@parent' => $parent_label]),
      ],
    ];
  }

  return $data;
}
