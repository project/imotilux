<?php

namespace Drupal\imotilux;

use Drupal\Core\Breadcrumb\Breadcrumb;
use Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\node\NodeInterface;

/**
 * Provides a breadcrumb builder for nodes in a imotilux.
 */
class ImotiluxBreadcrumbBuilder implements BreadcrumbBuilderInterface {
  use StringTranslationTrait;

  /**
   * The node storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $nodeStorage;

  /**
   * The current user account.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * Constructs the ImotiluxBreadcrumbBuilder.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user account.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, AccountInterface $account) {
    $this->nodeStorage = $entity_type_manager->getStorage('node');
    $this->account = $account;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    $node = $route_match->getParameter('node');
    return $node instanceof NodeInterface && !empty($node->imotilux);
  }

  /**
   * {@inheritdoc}
   */
  public function build(RouteMatchInterface $route_match) {
    $imotilux_nids = [];
    $breadcrumb = new Breadcrumb();

    $links = [Link::createFromRoute($this->t('Home'), '<front>')];
    $imotilux = $route_match->getParameter('node')->imotilux;
    $depth = 1;
    // We skip the current node.
    while (!empty($imotilux['p' . ($depth + 1)])) {
      $imotilux_nids[] = $imotilux['p' . $depth];
      $depth++;
    }
    $parent_imotilux = $this->nodeStorage->loadMultiple($imotilux_nids);
    if (count($parent_imotilux) > 0) {
      $depth = 1;
      while (!empty($imotilux['p' . ($depth + 1)])) {
        if (!empty($parent_imotilux[$imotilux['p' . $depth]]) && ($parent_imotilux = $parent_imotilux[$imotilux['p' . $depth]])) {
          $access = $parent_imotilux->access('view', $this->account, TRUE);
          $breadcrumb->addCacheableDependency($access);
          if ($access->isAllowed()) {
            $breadcrumb->addCacheableDependency($parent_imotilux);
            $links[] = Link::createFromRoute($parent_imotilux->label(), 'entity.node.canonical', ['node' => $parent_imotilux->id()]);
          }
        }
        $depth++;
      }
    }
    $breadcrumb->setLinks($links);
    $breadcrumb->addCacheContexts(['route.imotilux_navigation']);
    return $breadcrumb;
  }

}
