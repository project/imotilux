<?php

namespace Drupal\imotilux\Plugin\Validation\Constraint;

use Drupal\imotilux\ImotiluxManagerInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Constraint validator for changing the imotilux outline in pending revisions.
 */
class ImotiluxOutlineConstraintValidator extends ConstraintValidator implements ContainerInjectionInterface {

  /**
   * The imotilux manager.
   *
   * @var \Drupal\imotilux\ImotiluxManagerInterface
   */
  protected $imotiluxManager;

  /**
   * Creates a new ImotiluxOutlineConstraintValidator instance.
   *
   * @param \Drupal\imotilux\ImotiluxManagerInterface $imotilux_manager
   *   The imotilux manager.
   */
  public function __construct(ImotiluxManagerInterface $imotilux_manager) {
    $this->imotiluxManager = $imotilux_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('imotilux.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function validate($entity, Constraint $constraint) {
    if (isset($entity) && !$entity->isNew() && !$entity->isDefaultRevision()) {
      /** @var \Drupal\Core\Entity\ContentEntityInterface $original */
      $original = $this->imotiluxManager->loadImotiluxLink($entity->id(), FALSE) ?: [
        'bid' => 0,
        'weight' => 0,
      ];
      if (empty($original['pid'])) {
        $original['pid'] = -1;
      }

      if ($entity->imotilux['bid'] != $original['bid']) {
        $this->context->buildViolation($constraint->message)
          ->atPath('imotilux.bid')
          ->setInvalidValue($entity)
          ->addViolation();
      }
      if ($entity->imotilux['pid'] != $original['pid']) {
        $this->context->buildViolation($constraint->message)
          ->atPath('imotilux.pid')
          ->setInvalidValue($entity)
          ->addViolation();
      }
      if ($entity->imotilux['weight'] != $original['weight']) {
        $this->context->buildViolation($constraint->message)
          ->atPath('imotilux.weight')
          ->setInvalidValue($entity)
          ->addViolation();
      }
    }
  }

}
