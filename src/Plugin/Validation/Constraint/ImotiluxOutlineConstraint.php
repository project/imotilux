<?php

namespace Drupal\imotilux\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Validation constraint for changing the imotilux outline in pending revisions.
 *
 * @Constraint(
 *   id = "ImotiluxOutline",
 *   label = @Translation("Imotilux outline.", context = "Validation"),
 * )
 */
class ImotiluxOutlineConstraint extends Constraint {

  public $message = 'You can only change the imotilux outline for the <em>published</em> version of this content.';

}
