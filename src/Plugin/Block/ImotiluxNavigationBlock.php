<?php

namespace Drupal\imotilux\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\imotilux\ImotiluxManagerInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Entity\EntityStorageInterface;

/**
 * Provides a 'Imotilux navigation' block.
 *
 * @Block(
 *   id = "imotilux_navigation",
 *   admin_label = @Translation("Imotilux navigation"),
 *   category = @Translation("Menus")
 * )
 */
class ImotiluxNavigationBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The request object.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The imotilux manager.
   *
   * @var \Drupal\imotilux\ImotiluxManagerInterface
   */
  protected $imotiluxManager;

  /**
   * The node storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $nodeStorage;

  /**
   * Constructs a new ImotiluxNavigationBlock instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack object.
   * @param \Drupal\imotilux\ImotiluxManagerInterface $imotilux_manager
   *   The imotilux manager.
   * @param \Drupal\Core\Entity\EntityStorageInterface $node_storage
   *   The node storage.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, RequestStack $request_stack, ImotiluxManagerInterface $imotilux_manager, EntityStorageInterface $node_storage) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->requestStack = $request_stack;
    $this->imotiluxManager = $imotilux_manager;
    $this->nodeStorage = $node_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('request_stack'),
      $container->get('imotilux.manager'),
      $container->get('entity_type.manager')->getStorage('node')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'block_mode' => "all pages",
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $options = [
      'all pages' => $this->t('Show block on all pages'),
      'imotilux pages' => $this->t('Show block only on imotilux pages'),
    ];
    $form['imotilux_block_mode'] = [
      '#type' => 'radios',
      '#title' => $this->t('Imotilux navigation block display'),
      '#options' => $options,
      '#default_value' => $this->configuration['block_mode'],
      '#description' => $this->t("If <em>Show block on all pages</em> is selected, the block will contain the automatically generated menus for all of the site's imotilux. If <em>Show block only on imotilux pages</em> is selected, the block will contain only the one menu corresponding to the current page's imotilux. In this case, if the current page is not in a imotilux, no block will be displayed. The <em>Page specific visibility settings</em> or other visibility settings can be used in addition to selectively display this block."),
      ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['block_mode'] = $form_state->getValue('imotilux_block_mode');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $current_bid = 0;

    if ($node = $this->requestStack->getCurrentRequest()->get('node')) {
      $current_bid = empty($node->imotilux['bid']) ? 0 : $node->imotilux['bid'];
    }
    if ($this->configuration['block_mode'] == 'all pages') {
      $imotilux_menus = [];
      $pseudo_tree = [0 => ['below' => FALSE]];
      foreach ($this->imotiluxManager->getAllImotilux() as $imotilux_id => $imotilux) {
        if ($imotilux['bid'] == $current_bid) {
          // If the current page is a node associated with a imotilux, the menu
          // needs to be retrieved.
          $data = $this->imotiluxManager->imotiluxTreeAllData($node->imotilux['bid'], $node->imotilux);
          $imotilux_menus[$imotilux_id] = $this->imotiluxManager->imotiluxTreeOutput($data);
        }
        else {
          // Since we know we will only display a link to the top node, there
          // is no reason to run an additional menu tree query for each imotilux.
          $imotilux['in_active_trail'] = FALSE;
          // Check whether user can access the imotilux link.
          $imotilux_node = $this->nodeStorage->load($imotilux['nid']);
          $imotilux['access'] = $imotilux_node->access('view');
          $pseudo_tree[0]['link'] = $imotilux;
          $imotilux_menus[$imotilux_id] = $this->imotiluxManager->imotiluxTreeOutput($pseudo_tree);
        }
        $imotilux_menus[$imotilux_id] += [
          '#imotilux_title' => $imotilux['title'],
        ];
      }
      if ($imotilux_menus) {
        return [
          '#theme' => 'imotilux_all_imotilux_block',
        ] + $imotilux_menus;
      }
    }
    elseif ($current_bid) {
      // Only display this block when the user is browsing a imotilux and do
      // not show unpublished imotilux.
      $nid = \Drupal::entityQuery('node')
        ->condition('nid', $node->imotilux['bid'], '=')
        ->condition('status', NodeInterface::PUBLISHED)
        ->execute();

      // Only show the block if the user has view access for the top-level node.
      if ($nid) {
        $tree = $this->imotiluxManager->imotiluxTreeAllData($node->imotilux['bid'], $node->imotilux);
        // There should only be one element at the top level.
        $data = array_shift($tree);
        $below = $this->imotiluxManager->imotiluxTreeOutput($data['below']);
        if (!empty($below)) {
          return $below;
        }
      }
    }
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), ['route.imotilux_navigation']);
  }

  /**
   * {@inheritdoc}
   *
   * @todo Make cacheable in https://www.drupal.org/node/2483181
   */
  public function getCacheMaxAge() {
    return 0;
  }

}
