<?php

namespace Drupal\imotilux\Controller;

use Drupal\imotilux\ImotiluxExport;
use Drupal\imotilux\ImotiluxManagerInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Controller routines for imotilux routes.
 */
class ImotiluxController extends ControllerBase {

  /**
   * The imotilux manager.
   *
   * @var \Drupal\imotilux\ImotiluxManagerInterface
   */
  protected $imotiluxManager;

  /**
   * The imotilux export service.
   *
   * @var \Drupal\imotilux\ImotiluxExport
   */
  protected $imotiluxExport;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructs a ImotiluxController object.
   *
   * @param \Drupal\imotilux\ImotiluxManagerInterface $imotiluxManager
   *   The imotilux manager.
   * @param \Drupal\imotilux\ImotiluxExport $imotiluxExport
   *   The imotilux export service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   */
  public function __construct(ImotiluxManagerInterface $imotiluxManager, ImotiluxExport $imotiluxExport, RendererInterface $renderer) {
    $this->imotiluxManager = $imotiluxManager;
    $this->imotiluxExport = $imotiluxExport;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('imotilux.manager'),
      $container->get('imotilux.export'),
      $container->get('renderer')
    );
  }

  /**
   * Returns an administrative overview of all imotilux.
   *
   * @return array
   *   A render array representing the administrative page content.
   */
  public function adminOverview() {
    $rows = [];

    $headers = [t('Imotilux'), t('Operations')];
    // Add any recognized imotilux to the table list.
    foreach ($this->imotiluxManager->getAllImotilux() as $imotilux) {
      /** @var \Drupal\Core\Url $url */
      $url = $imotilux['url'];
      if (isset($imotilux['options'])) {
        $url->setOptions($imotilux['options']);
      }
      $row = [
        Link::fromTextAndUrl($imotilux['title'], $url),
      ];
      $links = [];
      $links['edit'] = [
        'title' => t('Edit order and titles'),
        'url' => Url::fromRoute('imotilux.admin_edit', ['node' => $imotilux['nid']]),
      ];
      $row[] = [
        'data' => [
          '#type' => 'operations',
          '#links' => $links,
        ],
      ];
      $rows[] = $row;
    }
    return [
      '#type' => 'table',
      '#header' => $headers,
      '#rows' => $rows,
      '#empty' => t('No imotilux available.'),
    ];
  }

  /**
   * Prints a listing of all imotilux.
   *
   * @return array
   *   A render array representing the listing of all imotilux content.
   */
  public function imotiluxRender() {
    $imotilux_list = [];
    foreach ($this->imotiluxManager->getAllImotilux() as $imotilux) {
      $imotilux_list[] = Link::fromTextAndUrl($imotilux['title'], $imotilux['url']);
    }
    return [
      '#theme' => 'item_list',
      '#items' => $imotilux_list,
      '#cache' => [
        'tags' => $this->entityTypeManager()->getDefinition('node')->getListCacheTags(),
      ],
    ];
  }

  /**
   * Generates representations of a imotilux page and its children.
   *
   * The method delegates the generation of output to helper methods. The method
   * name is derived by prepending 'imotiluxExport' to the camelized form of given
   * output type. For example, a type of 'html' results in a call to the method
   * imotiluxExportHtml().
   *
   * @param string $type
   *   A string encoding the type of output requested. The following types are
   *   currently supported in imotilux module:
   *   - html: Printer-friendly HTML.
   *   Other types may be supported in contributed modules.
   * @param \Drupal\node\NodeInterface $node
   *   The node to export.
   *
   * @return array
   *   A render array representing the node and its children in the imotilux
   *   hierarchy in a format determined by the $type parameter.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
   */
  public function imotiluxExport($type, NodeInterface $node) {
    $method = 'imotiluxExport' . Container::camelize($type);

    // @todo Convert the custom export functionality to serializer.
    if (!method_exists($this->imotiluxExport, $method)) {
      $this->messenger()->addStatus(t('Unknown export format.'));
      throw new NotFoundHttpException();
    }

    $exported_imotilux = $this->imotiluxExport->{$method}($node);
    return new Response($this->renderer->renderRoot($exported_imotilux));
  }

}
