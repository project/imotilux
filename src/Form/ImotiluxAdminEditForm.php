<?php

namespace Drupal\imotilux\Form;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\imotilux\ImotiluxManager;
use Drupal\imotilux\ImotiluxManagerInterface;
use Drupal\Component\Utility\Crypt;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Url;
use Drupal\Core\Render\Renderer;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for administering a single imotilux's hierarchy.
 *
 * @internal
 */
class ImotiluxAdminEditForm extends FormBase {

  /**
   * The node storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $nodeStorage;

  /**
   * The imotilux manager.
   *
   * @var \Drupal\imotilux\ImotiluxManagerInterface
   */
  protected $imotiluxManager;

  /**
   * @var \Drupal\Core\Session\AccountProxyInterface $currentUser
   */
  protected $currentUser;

  /**
   * @var \Drupal\Core\Render\Renderer $renderer
   */
  protected $renderer;

  /**
   * Constructs a new ImotiluxAdminEditForm.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $node_storage
   *   The custom block storage.
   * @param \Drupal\imotilux\ImotiluxManagerInterface $imotilux_manager
   *   The imotilux manager.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   */
  public function __construct(EntityStorageInterface $node_storage, ImotiluxManagerInterface $imotilux_manager, AccountProxyInterface $current_user, Renderer $renderer) {
    $this->nodeStorage = $node_storage;
    $this->imotiluxManager = $imotilux_manager;
    $this->currentUser = $current_user;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $entity_manager = $container->get('entity_type.manager');
    return new static(
      $entity_manager->getStorage('node'),
      $container->get('imotilux.manager'),
      $container->get('current_user'),
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'imotilux_admin_edit';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, NodeInterface $node = NULL) {
    $form['#title'] = $node->label();
    $form['#node'] = $node;
    $this->imotiluxAdminTable($node, $form);
    $form['save'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save imotilux pages'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->getValue('tree_hash') != $form_state->getValue('tree_current_hash')) {
      $form_state->setErrorByName('', $this->t('This imotilux has been modified by another user, the changes could not be saved.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Save elements in the same order as defined in post rather than the form.
    // This ensures parents are updated before their children, preventing orphans.
    $user_input = $form_state->getUserInput();
    if (isset($user_input['table'])) {
      $order = array_flip(array_keys($user_input['table']));
      $form['table'] = array_merge($order, $form['table']);

      foreach (Element::children($form['table']) as $key) {
        if ($form['table'][$key]['#item']) {
          $row = $form['table'][$key];
          $values = $form_state->getValue(['table', $key]);

          // Update menu item if moved.
          if ($row['parent']['pid']['#default_value'] != $values['pid'] || $row['weight']['#default_value'] != $values['weight']) {
            $link = $this->imotiluxManager->loadImotiluxLink($values['nid'], FALSE);
            $link['weight'] = $values['weight'];
            $link['pid'] = $values['pid'];
            $this->imotiluxManager->saveImotiluxLink($link, FALSE);
          }

          // Update the title if changed.
          if ($row['title']['#default_value'] != $values['title']) {
            $node = $this->nodeStorage->load($values['nid']);
            $node->revision_log = $this->t('Title changed from %original to %current.', ['%original' => $node->label(), '%current' => $values['title']]);
            $node->title = $values['title'];
            $node->imotilux['link_title'] = $values['title'];
            $node->setNewRevision();
            $node->save();
            $this->logger('content')->notice('imotilux: updated %title.', ['%title' => $node->label(), 'link' => $node->toLink($this->t('View'))->toString()]);
          }
        }
      }
    }

    $this->messenger()->addStatus($this->t('Updated imotilux %title.', ['%title' => $form['#node']->label()]));
  }

  /**
   * Builds the table portion of the form for the imotilux administration page.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node of the top-level page in the imotilux.
   * @param array $form
   *   The form that is being modified, passed by reference.
   *
   * @see self::buildForm()
   */
  protected function imotiluxAdminTable(NodeInterface $node, array &$form) {
    $form['table'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Title'),
        $this->t('Weight'),
        $this->t('Parent'),
        $this->t('Operations'),
      ],
      '#empty' => $this->t('No imotilux content available.'),
      '#tabledrag' => [
        [
          'action' => 'match',
          'relationship' => 'parent',
          'group' => 'imotilux-pid',
          'subgroup' => 'imotilux-pid',
          'source' => 'imotilux-nid',
          'hidden' => TRUE,
          'limit' => ImotiluxManager::IMOTILUX_MAX_DEPTH - 2,
        ],
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'imotilux-weight',
        ],
      ],
    ];

    $tree = $this->imotiluxManager->imotiluxSubtreeData($node->imotilux);
    // Do not include the imotilux item itself.
    $tree = array_shift($tree);
    if ($tree['below']) {
      $hash = Crypt::hashBase64(serialize($tree['below']));
      // Store the hash value as a hidden form element so that we can detect
      // if another user changed the imotilux hierarchy.
      $form['tree_hash'] = [
        '#type' => 'hidden',
        '#default_value' => $hash,
      ];
      $form['tree_current_hash'] = [
        '#type' => 'value',
        '#value' => $hash,
      ];
      $this->imotiluxAdminTableTree($tree['below'], $form['table']);
    }
  }

  /**
   * Helps build the main table in the imotilux administration page form.
   *
   * @param array $tree
   *   A subtree of the imotilux menu hierarchy.
   * @param array $form
   *   The form that is being modified, passed by reference.
   *
   * @see self::buildForm()
   */
  protected function imotiluxAdminTableTree(array $tree, array &$form) {
    // The delta must be big enough to give each node a distinct value.
    $count = count($tree);
    $delta = ($count < 30) ? 15 : intval($count / 2) + 1;

    $access = $this->currentUser()->hasPermission('administer nodes');
    $destination = $this->getDestinationArray();

    foreach ($tree as $data) {
      $nid = $data['link']['nid'];
      $id = 'imotilux-admin-' . $nid;

      $form[$id]['#item'] = $data['link'];
      $form[$id]['#nid'] = $nid;
      $form[$id]['#attributes']['class'][] = 'draggable';
      $form[$id]['#weight'] = $data['link']['weight'];

      if (isset($data['link']['depth']) && $data['link']['depth'] > 2) {
        $indentation = [
          '#theme' => 'indentation',
          '#size' => $data['link']['depth'] - 2,
        ];
      }

      $form[$id]['title'] = [
        '#prefix' => !empty($indentation) ? $this->renderer->render($indentation) : '',
        '#type' => 'textfield',
        '#default_value' => $data['link']['title'],
        '#maxlength' => 255,
        '#size' => 40,
      ];

      $form[$id]['weight'] = [
        '#type' => 'weight',
        '#default_value' => $data['link']['weight'],
        '#delta' => max($delta, abs($data['link']['weight'])),
        '#title' => $this->t('Weight for @title', ['@title' => $data['link']['title']]),
        '#title_display' => 'invisible',
        '#attributes' => [
          'class' => ['imotilux-weight'],
        ],
      ];

      $form[$id]['parent']['nid'] = [
        '#parents' => ['table', $id, 'nid'],
        '#type' => 'hidden',
        '#value' => $nid,
        '#attributes' => [
          'class' => ['imotilux-nid'],
        ],
      ];

      $form[$id]['parent']['pid'] = [
        '#parents' => ['table', $id, 'pid'],
        '#type' => 'hidden',
        '#default_value' => $data['link']['pid'],
        '#attributes' => [
          'class' => ['imotilux-pid'],
        ],
      ];

      $form[$id]['parent']['bid'] = [
        '#parents' => ['table', $id, 'bid'],
        '#type' => 'hidden',
        '#default_value' => $data['link']['bid'],
        '#attributes' => [
          'class' => ['imotilux-bid'],
        ],
      ];

      $form[$id]['operations'] = [
        '#type' => 'operations',
      ];
      $form[$id]['operations']['#links']['view'] = [
        'title' => $this->t('View'),
        'url' => new Url('entity.node.canonical', ['node' => $nid]),
      ];

      if ($access) {
        $form[$id]['operations']['#links']['edit'] = [
          'title' => $this->t('Edit'),
          'url' => new Url('entity.node.edit_form', ['node' => $nid]),
          'query' => $destination,
        ];
        $form[$id]['operations']['#links']['delete'] = [
          'title' => $this->t('Delete'),
          'url' => new Url('entity.node.delete_form', ['node' => $nid]),
          'query' => $destination,
        ];
      }

      if ($data['below']) {
        $this->imotiluxAdminTableTree($data['below'], $form);
      }
    }
  }

}
