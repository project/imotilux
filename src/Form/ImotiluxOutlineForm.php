<?php

namespace Drupal\imotilux\Form;

use Drupal\imotilux\ImotiluxManagerInterface;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Displays the imotilux outline form.
 *
 * @internal
 */
class ImotiluxOutlineForm extends ContentEntityForm {

  /**
   * The imotilux being displayed.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $entity;

  /**
   * ImotiluxManager service.
   *
   * @var \Drupal\imotilux\ImotiluxManagerInterface
   */
  protected $imotiluxManager;

  /**
   * Constructs a ImotiluxOutlineForm object.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository.
   * @param \Drupal\imotilux\ImotiluxManagerInterface $imotilux_manager
   *   The ImotiluxManager service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(EntityRepositoryInterface $entity_repository, ImotiluxManagerInterface $imotilux_manager, EntityTypeBundleInfoInterface $entity_type_bundle_info = NULL, TimeInterface $time = NULL) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
    $this->imotiluxManager = $imotilux_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('imotilux.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getBaseFormId() {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form['#title'] = $this->entity->label();

    if (!isset($this->entity->imotilux)) {
      // The node is not part of any imotilux yet - set default options.
      $this->entity->imotilux = $this->imotiluxManager->getLinkDefaults($this->entity->id());
    }
    else {
      $this->entity->imotilux['original_bid'] = $this->entity->imotilux['bid'];
    }

    // Find the depth limit for the parent select.
    if (!isset($this->entity->imotilux['parent_depth_limit'])) {
      $this->entity->imotilux['parent_depth_limit'] = $this->imotiluxManager->getParentDepthLimit($this->entity->imotilux);
    }
    $form = $this->imotiluxManager->addFormElements($form, $form_state, $this->entity, $this->currentUser(), FALSE);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);
    $actions['submit']['#value'] = $this->entity->imotilux['original_bid'] ? $this->t('Update imotilux outline') : $this->t('Add to imotilux outline');
    $actions['delete']['#title'] = $this->t('Remove from imotilux outline');
    $actions['delete']['#url'] = new Url('entity.node.imotilux_remove_form', ['node' => $this->entity->imotilux['nid']]);
    $actions['delete']['#access'] = $this->imotiluxManager->checkNodeIsRemovable($this->entity);
    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $form_state->setRedirect(
      'entity.node.canonical',
      ['node' => $this->entity->id()]
    );
    $imotilux_link = $form_state->getValue('imotilux');
    if (!$imotilux_link['bid']) {
      $this->messenger()->addStatus($this->t('No changes were made'));
      return;
    }

    $this->entity->imotilux = $imotilux_link;
    if ($this->imotiluxManager->updateOutline($this->entity)) {
      if (isset($this->entity->imotilux['parent_mismatch']) && $this->entity->imotilux['parent_mismatch']) {
        // This will usually only happen when JS is disabled.
        $this->messenger()->addStatus($this->t('The post has been added to the selected imotilux. You may now position it relative to other pages.'));
        $form_state->setRedirectUrl($this->entity->toUrl('imotilux-outline-form'));
      }
      else {
        $this->messenger()->addStatus($this->t('The imotilux outline has been updated.'));
      }
    }
    else {
      $this->messenger()->addError($this->t('There was an error adding the post to the imotilux.'));
    }
  }

}
