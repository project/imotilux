<?php

namespace Drupal\imotilux\Form;

use Drupal\imotilux\ImotiluxManagerInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Remove form for imotilux module.
 *
 * @internal
 */
class ImotiluxRemoveForm extends ConfirmFormBase {

  /**
   * The imotilux manager.
   *
   * @var \Drupal\imotilux\ImotiluxManagerInterface
   */
  protected $imotiluxManager;

  /**
   * The node representing the imotilux.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $node;

  /**
   * Constructs a ImotiluxRemoveForm object.
   *
   * @param \Drupal\imotilux\ImotiluxManagerInterface $imotilux_manager
   *   The imotilux manager.
   */
  public function __construct(ImotiluxManagerInterface $imotilux_manager) {
    $this->imotiluxManager = $imotilux_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('imotilux.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'imotilux_remove_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, NodeInterface $node = NULL) {
    $this->node = $node;
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    $title = ['%title' => $this->node->label()];
    if ($this->node->imotilux['has_children']) {
      return $this->t('%title has associated child pages, which will be relocated automatically to maintain their connection to the imotilux. To recreate the hierarchy (as it was before removing this page), %title may be added again using the Outline tab, and each of its former child pages will need to be relocated manually.', $title);
    }
    else {
      return $this->t('%title may be added to hierarchy again using the Outline tab.', $title);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Remove');
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to remove %title from the imotilux hierarchy?', ['%title' => $this->node->label()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return $this->node->toUrl();
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($this->imotiluxManager->checkNodeIsRemovable($this->node)) {
      $this->imotiluxManager->deleteFromImotilux($this->node->id());
      $this->messenger()->addStatus($this->t('The post has been removed from the imotilux.'));
    }
    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
