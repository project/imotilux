<?php

namespace Drupal\imotilux\Access;

use Drupal\imotilux\ImotiluxManagerInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\node\NodeInterface;

/**
 * Determines whether the requested node can be removed from its imotilux.
 */
class ImotiluxNodeIsRemovableAccessCheck implements AccessInterface {

  /**
   * Imotilux Manager Service.
   *
   * @var \Drupal\imotilux\ImotiluxManagerInterface
   */
  protected $imotiluxManager;

  /**
   * Constructs a ImotiluxNodeIsRemovableAccessCheck object.
   *
   * @param \Drupal\imotilux\ImotiluxManagerInterface $imotilux_manager
   *   Imotilux Manager Service.
   */
  public function __construct(ImotiluxManagerInterface $imotilux_manager) {
    $this->imotiluxManager = $imotilux_manager;
  }

  /**
   * Checks access for removing the node from its imotilux.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node requested to be removed from its imotilux.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(NodeInterface $node) {
    return AccessResult::allowedIf($this->imotiluxManager->checkNodeIsRemovable($node))->addCacheableDependency($node);
  }

}
