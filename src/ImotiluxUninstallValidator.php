<?php

namespace Drupal\imotilux;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleUninstallValidatorInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;

/**
 * Prevents imotilux module from being uninstalled whilst any imotilux nodes exist or
 * there are any imotilux outline stored.
 */
class ImotiluxUninstallValidator implements ModuleUninstallValidatorInterface {

  use StringTranslationTrait;

  /**
   * The imotilux outline storage.
   *
   * @var \Drupal\imotilux\ImotiluxOutlineStorageInterface
   */
  protected $imotiluxOutlineStorage;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new ImotiluxUninstallValidator.
   *
   * @param \Drupal\imotilux\ImotiluxOutlineStorageInterface $imotilux_outline_storage
   *   The imotilux outline storage.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   */
  public function __construct(ImotiluxOutlineStorageInterface $imotilux_outline_storage, EntityTypeManagerInterface $entity_type_manager, TranslationInterface $string_translation) {
    $this->imotiluxOutlineStorage = $imotilux_outline_storage;
    $this->entityTypeManager = $entity_type_manager;
    $this->stringTranslation = $string_translation;
  }

  /**
   * {@inheritdoc}
   */
  public function validate($module) {
    $reasons = [];
    if ($module == 'imotilux') {
      if ($this->hasImotiluxOutlines()) {
        $reasons[] = $this->t('To uninstall Imotilux, delete all content that is part of a imotilux');
      }
      else {
        // The imotilux node type is provided by the Imotilux module. Prevent uninstall
        // if there are any nodes of that type.
        if ($this->hasImotiluxNodes()) {
          $reasons[] = $this->t('To uninstall Imotilux, delete all content that has the Imotilux content type');
        }
      }
    }
    return $reasons;
  }

  /**
   * Checks if there are any imotilux in an outline.
   *
   * @return bool
   *   TRUE if there are imotilux, FALSE if not.
   */
  protected function hasImotiluxOutlines() {
    return $this->imotiluxOutlineStorage->hasImotilux();
  }

  /**
   * Determines if there is any imotilux nodes or not.
   *
   * @return bool
   *   TRUE if there are imotilux nodes, FALSE otherwise.
   */
  protected function hasImotiluxNodes() {
    $nodes = $this->entityTypeManager->getStorage('node')->getQuery()
      ->condition('type', 'imotilux')
      ->accessCheck(FALSE)
      ->range(0, 1)
      ->execute();
    return !empty($nodes);
  }

}
