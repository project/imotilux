<?php

namespace Drupal\imotilux;

/**
 * Provides handling to render the imotilux outline.
 */
class ImotiluxOutline {

  /**
   * The imotilux manager.
   *
   * @var \Drupal\imotilux\ImotiluxManagerInterface
   */
  protected $imotiluxManager;

  /**
   * Constructs a new ImotiluxOutline.
   *
   * @param \Drupal\imotilux\ImotiluxManagerInterface $imotilux_manager
   *   The imotilux manager.
   */
  public function __construct(ImotiluxManagerInterface $imotilux_manager) {
    $this->imotiluxManager = $imotilux_manager;
  }

  /**
   * Fetches the imotilux link for the previous page of the imotilux.
   *
   * @param array $imotilux_link
   *   A fully loaded imotilux link that is part of the imotilux hierarchy.
   *
   * @return array
   *   A fully loaded imotilux link for the page before the one represented in
   *   $imotilux_link.
   */
  public function prevLink(array $imotilux_link) {
    // If the parent is zero, we are at the start of a imotilux.
    if ($imotilux_link['pid'] == 0) {
      return NULL;
    }
    $flat = $this->imotiluxManager->imotiluxTreeGetFlat($imotilux_link);
    reset($flat);
    $curr = NULL;
    do {
      $prev = $curr;
      $key = key($flat);
      $curr = current($flat);
      next($flat);
    } while ($key && $key != $imotilux_link['nid']);

    if ($key == $imotilux_link['nid']) {
      // The previous page in the imotilux may be a child of the previous visible link.
      if ($prev['depth'] == $imotilux_link['depth']) {
        // The subtree will have only one link at the top level - get its data.
        $tree = $this->imotiluxManager->imotiluxSubtreeData($prev);
        $data = array_shift($tree);
        // The link of interest is the last child - iterate to find the deepest one.
        while ($data['below']) {
          $data = end($data['below']);
        }
        $this->imotiluxManager->imotiluxLinkTranslate($data['link']);
        return $data['link'];
      }
      else {
        $this->imotiluxManager->imotiluxLinkTranslate($prev);
        return $prev;
      }
    }
  }

  /**
   * Fetches the imotilux link for the next page of the imotilux.
   *
   * @param array $imotilux_link
   *   A fully loaded imotilux link that is part of the imotilux hierarchy.
   *
   * @return array
   *   A fully loaded imotilux link for the page after the one represented in
   *   $imotilux_link.
   */
  public function nextLink(array $imotilux_link) {
    $flat = $this->imotiluxManager->imotiluxTreeGetFlat($imotilux_link);
    reset($flat);
    do {
      $key = key($flat);
      next($flat);
    } while ($key && $key != $imotilux_link['nid']);

    if ($key == $imotilux_link['nid']) {
      $next = current($flat);
      if ($next) {
        $this->imotiluxManager->imotiluxLinkTranslate($next);
      }
      return $next;
    }
  }

  /**
   * Formats the imotilux links for the child pages of the current page.
   *
   * @param array $imotilux_link
   *   A fully loaded imotilux link that is part of the imotilux hierarchy.
   *
   * @return array
   *   HTML for the links to the child pages of the current page.
   */
  public function childrenLinks(array $imotilux_link) {
    $flat = $this->imotiluxManager->imotiluxTreeGetFlat($imotilux_link);

    $children = [];

    if ($imotilux_link['has_children']) {
      // Walk through the array until we find the current page.
      do {
        $link = array_shift($flat);
      } while ($link && ($link['nid'] != $imotilux_link['nid']));
      // Continue though the array and collect the links whose parent is this page.
      while (($link = array_shift($flat)) && $link['pid'] == $imotilux_link['nid']) {
        $data['link'] = $link;
        $data['below'] = '';
        $children[] = $data;
      }
    }

    if ($children) {
      return $this->imotiluxManager->imotiluxTreeOutput($children);
    }
    return '';
  }

}
