<?php

namespace Drupal\imotilux\Cache;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\Context\CacheContextInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Defines the imotilux navigation cache context service.
 *
 * Cache context ID: 'route.imotilux_navigation'.
 *
 * This allows for imotilux navigation location-aware caching. It depends on:
 * - whether the current route represents a imotilux node at all
 * - and if so, where in the imotilux hierarchy we are
 *
 * This class is container-aware to avoid initializing the 'imotilux.manager'
 * service when it is not necessary.
 */
class ImotiluxNavigationCacheContext implements CacheContextInterface, ContainerAwareInterface {

  use ContainerAwareTrait;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Constructs a new ImotiluxNavigationCacheContext service.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(RequestStack $request_stack) {
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public static function getLabel() {
    return t("Imotilux navigation");
  }

  /**
   * {@inheritdoc}
   */
  public function getContext() {
    // Find the current imotilux's ID.
    $current_bid = 0;
    if ($node = $this->requestStack->getCurrentRequest()->get('node')) {
      $current_bid = empty($node->imotilux['bid']) ? 0 : $node->imotilux['bid'];
    }

    // If we're not looking at a imotilux node, then we're not navigating a imotilux.
    if ($current_bid === 0) {
      return 'imotilux.none';
    }

    // If we're looking at a imotilux node, get the trail for that node.
    $active_trail = $this->container->get('imotilux.manager')
      ->getActiveTrailIds($node->imotilux['bid'], $node->imotilux);
    return implode('|', $active_trail);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata() {
    // The imotilux active trail depends on the node and data attached to it.
    // That information is however not stored as part of the node.
    $cacheable_metadata = new CacheableMetadata();
    if ($node = $this->requestStack->getCurrentRequest()->get('node')) {
      // If the node is part of a imotilux then we can use the cache tag for that
      // imotilux. If not, then it can't be optimized away.
      if (!empty($node->imotilux['bid'])) {
        $cacheable_metadata->addCacheTags(['bid:' . $node->imotilux['bid']]);
      }
      else {
        $cacheable_metadata->setCacheMaxAge(0);
      }
    }
    return $cacheable_metadata;
  }

}
