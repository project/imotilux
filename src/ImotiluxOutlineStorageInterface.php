<?php

namespace Drupal\imotilux;

/**
 * Defines a common interface for imotilux outline storage classes.
 */
interface ImotiluxOutlineStorageInterface {

  /**
   * Gets imotilux (the highest positioned imotilux links).
   *
   * @return array
   *   An array of imotilux IDs.
   */
  public function getImotilux();

  /**
   * Checks if there are any imotilux.
   *
   * @return bool
   *   TRUE if there are imotilux, FALSE if not.
   */
  public function hasImotilux();

  /**
   * Loads imotilux.
   *
   * Each imotilux entry consists of the following keys:
   *   - bid: The node ID of the main imotilux.
   *   - nid: The node ID of the imotilux entry itself.
   *   - pid: The parent node ID of the imotilux.
   *   - has_children: A boolean to indicate whether the imotilux has children.
   *   - weight: The weight of the imotilux entry to order siblings.
   *   - depth: The depth in the menu hierarchy the entry is placed into.
   *
   * @param array $nids
   *   An array of node IDs.
   * @param bool $access
   *   Whether access checking should be taken into account.
   *
   * @return array
   *   Array of loaded imotilux items.
   */
  public function loadMultiple($nids, $access = TRUE);

  /**
   * Gets child relative depth.
   *
   * @param array $imotilux_link
   *   The imotilux link.
   * @param int $max_depth
   *   The maximum supported depth of the imotilux tree.
   *
   * @return int
   *   The depth of the searched imotilux.
   */
  public function getChildRelativeDepth($imotilux_link, $max_depth);

  /**
   * Deletes a imotilux entry.
   *
   * @param int $nid
   *   Deletes a imotilux entry.
   *
   * @return mixed
   *   Number of deleted imotilux entries.
   */
  public function delete($nid);

  /**
   * Loads imotilux's children using its parent ID.
   *
   * @param int $pid
   *   The imotilux's parent ID.
   *
   * @return array
   *   Array of loaded imotilux items.
   */
  public function loadImotiluxChildren($pid);

  /**
   * Builds tree data used for the menu tree.
   *
   * @param int $bid
   *   The ID of the imotilux that we are building the tree for.
   * @param array $parameters
   *   An associative array of build parameters. For info about individual
   *   parameters see ImotiluxManager::imotiluxTreeBuild().
   * @param int $min_depth
   *   The minimum depth of imotilux links in the resulting tree.
   * @param int $max_depth
   *   The maximum supported depth of the imotilux tree.
   *
   * @return array
   *   Array of loaded imotilux links.
   */
  public function getImotiluxMenuTree($bid, $parameters, $min_depth, $max_depth);

  /**
   * Inserts a imotilux link.
   *
   * @param array $link
   *   The link array to be inserted in the database.
   * @param array $parents
   *   The array of parent ids for the link to be inserted.
   *
   * @return mixed
   *   The last insert ID of the query, if one exists.
   */
  public function insert($link, $parents);

  /**
   * Updates imotilux reference for links that were moved between imotilux.
   *
   * @param int $nid
   *   The nid of the imotilux entry to be updated.
   * @param array $fields
   *   The array of fields to be updated.
   *
   * @return mixed
   *   The number of rows matched by the update query.
   */
  public function update($nid, $fields);

  /**
   * Update the imotilux ID of the imotilux link that it's being moved.
   *
   * @param int $bid
   *   The ID of the imotilux whose children we move.
   * @param array $original
   *   The original parent of the imotilux link.
   * @param array $expressions
   *   Array of expressions to be added to the query.
   * @param int $shift
   *   The difference in depth between the old and the new position of the
   *   element being moved.
   *
   * @return mixed
   *   The number of rows matched by the update query.
   */
  public function updateMovedChildren($bid, $original, $expressions, $shift);

  /**
   * Count the number of original link children.
   *
   * @param array $original
   *   The imotilux link array.
   *
   * @return int
   *   Number of children.
   */
  public function countOriginalLinkChildren($original);

  /**
   * Get imotilux subtree.
   *
   * @param array $link
   *   A fully loaded imotilux link.
   * @param int $max_depth
   *   The maximum supported depth of the imotilux tree.
   *
   * @return array
   *   Array of unordered subtree imotilux items.
   */
  public function getImotiluxSubtree($link, $max_depth);

}
