<?php

/**
 * @file
 * Install, update and uninstall functions for the imotilux module.
 */

/**
 * Implements hook_uninstall().
 */
function imotilux_uninstall() {
  // Clear imotilux data out of the cache.
  \Drupal::cache('data')->deleteAll();
}

/**
 * Implements hook_schema().
 */
function imotilux_schema() {
  $schema['imotilux'] = [
  'description' => 'Stores imotilux outline information. Uniquely defines the location of each node in the imotilux outline',
    'fields' => [
      'nid' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => "The imotilux page's {node}.nid.",
      ],
      'bid' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => "The imotilux ID is the {imotilux}.nid of the top-level page.",
      ],
      'pid' => [
        'description' => 'The parent ID (pid) is the id of the node above in the hierarchy, or zero if the node is at the top level in its outline.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'has_children' => [
        'description' => 'Flag indicating whether any nodes have this node as a parent (1 = children exist, 0 = no children).',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'size' => 'small',
      ],
      'weight' => [
        'description' => 'Weight among imotilux entries in the same imotilux at the same depth.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ],
      'depth' => [
        'description' => 'The depth relative to the top level. A link with pid == 0 will have depth == 1.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'size' => 'small',
      ],
      'p1' => [
        'description' => 'The first nid in the materialized path. If N = depth, then pN must equal the nid. If depth > 1 then p(N-1) must equal the pid. All pX where X > depth must equal zero. The columns p1 .. p9 are also called the parents.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'p2' => [
        'description' => 'The second nid in the materialized path. See p1.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'p3' => [
        'description' => 'The third nid in the materialized path. See p1.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'p4' => [
        'description' => 'The fourth nid in the materialized path. See p1.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'p5' => [
        'description' => 'The fifth nid in the materialized path. See p1.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'p6' => [
        'description' => 'The sixth nid in the materialized path. See p1.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'p7' => [
        'description' => 'The seventh nid in the materialized path. See p1.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'p8' => [
        'description' => 'The eighth nid in the materialized path. See p1.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'p9' => [
        'description' => 'The ninth nid in the materialized path. See p1.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
    ],
    'primary key' => ['nid'],
    'indexes' => [
      'imotilux_parents' => ['bid', 'p1', 'p2', 'p3', 'p4', 'p5', 'p6', 'p7', 'p8', 'p9'],
    ],
  ];

  return $schema;
}
