<?php

namespace Drupal\Tests\imotilux\Functional;

use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides common functionality for Imotilux test classes.
 */
trait ImotiluxTestTrait {

  /**
   * A imotilux node.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $imotilux;

  /**
   * A user with permission to create and edit imotilux.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $imotiluxAuthor;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Creates a new imotilux with a page hierarchy.
   *
   * @param array $edit
   *   (optional) Field data in an associative array. Changes the current input
   *   fields (where possible) to the values indicated. Defaults to an empty
   *   array.
   *
   * @return \Drupal\node\NodeInterface[]
   */
  public function createImotilux($edit = []) {
    // Create new imotilux.
    $this->drupalLogin($this->imotiluxAuthor);

    $this->imotilux = $this->createImotiluxNode('new', NULL, $edit);
    $imotilux = $this->imotilux;

    /*
     * Add page hierarchy to imotilux.
     * Imotilux
     *  |- Node 0
     *   |- Node 1
     *   |- Node 2
     *  |- Node 3
     *  |- Node 4
     */
    $nodes = [];
    // Node 0.
    $nodes[] = $this->createImotiluxNode($imotilux->id(), NULL, $edit);
    // Node 1.
    $nodes[] = $this->createImotiluxNode($imotilux->id(), $nodes[0]->imotilux['nid'], $edit);
    // Node 2.
    $nodes[] = $this->createImotiluxNode($imotilux->id(), $nodes[0]->imotilux['nid'], $edit);
    // Node 3.
    $nodes[] = $this->createImotiluxNode($imotilux->id(), NULL, $edit);
    // Node 4.
    $nodes[] = $this->createImotiluxNode($imotilux->id(), NULL, $edit);

    $this->drupalLogout();

    return $nodes;
  }

  /**
   * Checks the outline of sub-pages; previous, up, and next.
   *
   * Also checks the printer friendly version of the outline.
   *
   * @param \Drupal\Core\Entity\EntityInterface $node
   *   Node to check.
   * @param $nodes
   *   Nodes that should be in outline.
   * @param $previous
   *   Previous link node.
   * @param $up
   *   Up link node.
   * @param $next
   *   Next link node.
   * @param array $breadcrumb
   *   The nodes that should be displayed in the breadcrumb.
   */
  public function checkImotiluxNode(EntityInterface $node, $nodes, $previous, $up, $next, array $breadcrumb) {
    // $number does not use drupal_static as it should not be reset
    // since it uniquely identifies each call to checkImotiluxNode().
    static $number = 0;
    $this->drupalGet('node/' . $node->id());

    // Check outline structure.
    if ($nodes !== NULL) {
      $this->assertPattern($this->generateOutlinePattern($nodes), new FormattableMarkup('Node @number outline confirmed.', ['@number' => $number]));
    }
    else {
      $this->pass(new FormattableMarkup('Node %number does not have outline.', ['%number' => $number]));
    }

    // Check previous, up, and next links.
    if ($previous) {
      /** @var \Drupal\Core\Url $url */
      $url = $previous->toUrl();
      $url->setOptions(['attributes' => ['rel' => ['prev'], 'title' => t('Go to previous page')]]);
      $text = new FormattableMarkup('<b>‹</b> @label', ['@label' => $previous->label()]);
      $this->assertRaw(Link::fromTextAndUrl($text, $url), 'Previous page link found.');
    }

    if ($up) {
      /** @var \Drupal\Core\Url $url */
      $url = $up->toUrl();
      $url->setOptions(['attributes' => ['title' => t('Go to parent page')]]);
      $this->assertRaw(Link::fromTextAndUrl('Up', $url), 'Up page link found.');
    }

    if ($next) {
      /** @var \Drupal\Core\Url $url */
      $url = $next->toUrl();
      $url->setOptions(['attributes' => ['rel' => ['next'], 'title' => t('Go to next page')]]);
      $text = new FormattableMarkup('@label <b>›</b>', ['@label' => $next->label()]);
      $this->assertRaw(Link::fromTextAndUrl($text, $url), 'Next page link found.');
    }

    // Compute the expected breadcrumb.
    $expected_breadcrumb = [];
    $expected_breadcrumb[] = Url::fromRoute('<front>')->toString();
    foreach ($breadcrumb as $a_node) {
      $expected_breadcrumb[] = $a_node->toUrl()->toString();
    }

    // Fetch links in the current breadcrumb.
    $links = $this->xpath('//nav[@class="breadcrumb"]/ol/li/a');
    $got_breadcrumb = [];
    foreach ($links as $link) {
      $got_breadcrumb[] = $link->getAttribute('href');
    }

    // Compare expected and got breadcrumbs.
    $this->assertIdentical($expected_breadcrumb, $got_breadcrumb, 'The breadcrumb is correctly displayed on the page.');

    // Check printer friendly version.
    $this->drupalGet('imotilux/export/html/' . $node->id());
    $this->assertText($node->label(), 'Printer friendly title found.');
    $this->assertRaw($node->body->processed, 'Printer friendly body found.');

    $number++;
  }

  /**
   * Creates a regular expression to check for the sub-nodes in the outline.
   *
   * @param array $nodes
   *   An array of nodes to check in outline.
   *
   * @return string
   *   A regular expression that locates sub-nodes of the outline.
   */
  public function generateOutlinePattern($nodes) {
    $outline = '';
    foreach ($nodes as $node) {
      $outline .= '(node\/' . $node->id() . ')(.*?)(' . $node->label() . ')(.*?)';
    }

    return '/<nav id="imotilux-navigation-' . $this->imotilux->id() . '"(.*?)<ul(.*?)' . $outline . '<\/ul>/s';
  }

  /**
   * Creates a imotilux node.
   *
   * @param int|string $imotilux_nid
   *   A imotilux node ID or set to 'new' to create a new imotilux.
   * @param int|null $parent
   *   (optional) Parent imotilux reference ID. Defaults to NULL.
   * @param array $edit
   *   (optional) Field data in an associative array. Changes the current input
   *   fields (where possible) to the values indicated. Defaults to an empty
   *   array.
   *
   * @return \Drupal\node\NodeInterface
   *   The created node.
   */
  public function createImotiluxNode($imotilux_nid, $parent = NULL, $edit = []) {
    // $number does not use drupal_static as it should not be reset
    // since it uniquely identifies each call to createImotiluxNode().
    // Used to ensure that when sorted nodes stay in same order.
    static $number = 0;

    $edit['title[0][value]'] = str_pad($number, 2, '0', STR_PAD_LEFT) . ' - SimpleTest test node ' . $this->randomMachineName(10);
    $edit['body[0][value]'] = 'SimpleTest test body ' . $this->randomMachineName(32) . ' ' . $this->randomMachineName(32);
    $edit['imotilux[bid]'] = $imotilux_nid;

    if ($parent !== NULL) {
      $this->drupalPostForm('node/add/imotilux', $edit, t('Change imotilux (update list of parents)'));

      $edit['imotilux[pid]'] = $parent;
      $this->drupalPostForm(NULL, $edit, t('Save'));
      // Make sure the parent was flagged as having children.
      $parent_node = $this->entityTypeManager->getStorage('node')->loadUnchanged($parent);
      $this->assertFalse(empty($parent_node->imotilux['has_children']), 'Parent node is marked as having children');
    }
    else {
      $this->drupalPostForm('node/add/imotilux', $edit, t('Save'));
    }

    // Check to make sure the imotilux node was created.
    $node = $this->drupalGetNodeByTitle($edit['title[0][value]']);
    $this->assertNotNull(($node === FALSE ? NULL : $node), 'Imotilux node found in database.');
    $number++;

    return $node;
  }

}
