<?php

namespace Drupal\Tests\imotilux\Functional;

use Drupal\Core\Cache\Cache;
use Drupal\Tests\BrowserTestBase;
use Drupal\user\RoleInterface;
use Drupal\Component\Render\FormattableMarkup;

/**
 * Create a imotilux, add pages, and test imotilux interface.
 *
 * @group imotilux
 */
class ImotiluxTest extends BrowserTestBase {

  use ImotiluxTestTrait;

  /**
   * Modules to install.
   *
   * @var array
   */
  public static $modules = ['imotilux', 'block', 'node_access_test', 'imotilux_test'];

  /**
   * A user with permission to view a imotilux and access printer-friendly version.
   *
   * @var object
   */
  protected $webUser;

  /**
   * A user with permission to create and edit imotilux and to administer blocks.
   *
   * @var object
   */
  protected $adminUser;

  /**
   * A user without the 'node test view' permission.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $webUserWithoutNodeAccess;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->drupalPlaceBlock('system_breadcrumb_block');
    $this->drupalPlaceBlock('page_title_block');

    // node_access_test requires a node_access_rebuild().
    node_access_rebuild();

    // Create users.
    $this->imotiluxAuthor = $this->drupalCreateUser(['create new imotilux', 'create imotilux content', 'edit own imotilux content', 'add content to imotilux']);
    $this->webUser = $this->drupalCreateUser(['access printer-friendly version', 'node test view']);
    $this->webUserWithoutNodeAccess = $this->drupalCreateUser(['access printer-friendly version']);
    $this->adminUser = $this->drupalCreateUser(['create new imotilux', 'create imotilux content', 'edit any imotilux content', 'delete any imotilux content', 'add content to imotilux', 'administer blocks', 'administer permissions', 'administer imotilux outlines', 'node test view', 'administer content types', 'administer site configuration']);
  }

  /**
   * Tests the imotilux navigation cache context.
   *
   * @see \Drupal\imotilux\Cache\ImotiluxNavigationCacheContext
   */
  public function testImotiluxNavigationCacheContext() {
    // Create a page node.
    $this->drupalCreateContentType(['type' => 'page']);
    $page = $this->drupalCreateNode();

    // Create a imotilux, consisting of imotilux nodes.
    $imotilux_nodes = $this->createImotilux();

    // Enable the debug output.
    \Drupal::state()->set('imotilux_test.debug_imotilux_navigation_cache_context', TRUE);
    Cache::invalidateTags(['imotilux_test.debug_imotilux_navigation_cache_context']);

    $this->drupalLogin($this->imotiluxAuthor);

    // On non-node route.
    $this->drupalGet($this->adminUser->toUrl());
    $this->assertRaw('[route.imotilux_navigation]=imotilux.none');

    // On non-imotilux node route.
    $this->drupalGet($page->toUrl());
    $this->assertRaw('[route.imotilux_navigation]=imotilux.none');

    // On imotilux node route.
    $this->drupalGet($imotilux_nodes[0]->toUrl());
    $this->assertRaw('[route.imotilux_navigation]=0|2|3');
    $this->drupalGet($imotilux_nodes[1]->toUrl());
    $this->assertRaw('[route.imotilux_navigation]=0|2|3|4');
    $this->drupalGet($imotilux_nodes[2]->toUrl());
    $this->assertRaw('[route.imotilux_navigation]=0|2|3|5');
    $this->drupalGet($imotilux_nodes[3]->toUrl());
    $this->assertRaw('[route.imotilux_navigation]=0|2|6');
    $this->drupalGet($imotilux_nodes[4]->toUrl());
    $this->assertRaw('[route.imotilux_navigation]=0|2|7');
  }

  /**
   * Tests saving the imotilux outline on an empty imotilux.
   */
  public function testEmptyImotilux() {
    // Create a new empty imotilux.
    $this->drupalLogin($this->imotiluxAuthor);
    $imotilux = $this->createImotiluxNode('new');
    $this->drupalLogout();

    // Log in as a user with access to the imotilux outline and save the form.
    $this->drupalLogin($this->adminUser);
    $this->drupalPostForm('admin/structure/imotilux/' . $imotilux->id(), [], t('Save imotilux pages'));
    $this->assertText(t('Updated imotilux @imotilux.', ['@imotilux' => $imotilux->label()]));
  }

  /**
   * Tests imotilux functionality through node interfaces.
   */
  public function testImotilux() {
    // Create new imotilux.
    $nodes = $this->createImotilux();
    $imotilux = $this->imotilux;

    $this->drupalLogin($this->webUser);

    // Check that imotilux pages display along with the correct outlines and
    // previous/next links.
    $this->checkImotiluxNode($imotilux, [$nodes[0], $nodes[3], $nodes[4]], FALSE, FALSE, $nodes[0], []);
    $this->checkImotiluxNode($nodes[0], [$nodes[1], $nodes[2]], $imotilux, $imotilux, $nodes[1], [$imotilux]);
    $this->checkImotiluxNode($nodes[1], NULL, $nodes[0], $nodes[0], $nodes[2], [$imotilux, $nodes[0]]);
    $this->checkImotiluxNode($nodes[2], NULL, $nodes[1], $nodes[0], $nodes[3], [$imotilux, $nodes[0]]);
    $this->checkImotiluxNode($nodes[3], NULL, $nodes[2], $imotilux, $nodes[4], [$imotilux]);
    $this->checkImotiluxNode($nodes[4], NULL, $nodes[3], $imotilux, FALSE, [$imotilux]);

    $this->drupalLogout();
    $this->drupalLogin($this->imotiluxAuthor);

    // Check the presence of expected cache tags.
    $this->drupalGet('node/add/imotilux');
    $this->assertCacheTag('config:imotilux.settings');

    /*
     * Add Node 5 under Node 3.
     * Imotilux
     *  |- Node 0
     *   |- Node 1
     *   |- Node 2
     *  |- Node 3
     *   |- Node 5
     *  |- Node 4
     */
    // Node 5.
    $nodes[] = $this->createImotiluxNode($imotilux->id(), $nodes[3]->imotilux['nid']);
    $this->drupalLogout();
    $this->drupalLogin($this->webUser);
    // Verify the new outline - make sure we don't get stale cached data.
    $this->checkImotiluxNode($nodes[3], [$nodes[5]], $nodes[2], $imotilux, $nodes[5], [$imotilux]);
    $this->checkImotiluxNode($nodes[4], NULL, $nodes[5], $imotilux, FALSE, [$imotilux]);
    $this->drupalLogout();
    // Create a second imotilux, and move an existing imotilux page into it.
    $this->drupalLogin($this->imotiluxAuthor);
    $other_imotilux = $this->createImotiluxNode('new');
    $node = $this->createImotiluxNode($imotilux->id());
    $edit = ['imotilux[bid]' => $other_imotilux->id()];
    $this->drupalPostForm('node/' . $node->id() . '/edit', $edit, t('Save'));

    $this->drupalLogout();
    $this->drupalLogin($this->webUser);

    // Check that the nodes in the second imotilux are displayed correctly.
    // First we must set $this->imotilux to the second imotilux, so that the
    // correct regex will be generated for testing the outline.
    $this->imotilux = $other_imotilux;
    $this->checkImotiluxNode($other_imotilux, [$node], FALSE, FALSE, $node, []);
    $this->checkImotiluxNode($node, NULL, $other_imotilux, $other_imotilux, FALSE, [$other_imotilux]);

    // Test that we can save a imotilux programmatically.
    $this->drupalLogin($this->imotiluxAuthor);
    $imotilux = $this->createImotiluxNode('new');
    $imotilux->save();
  }

  /**
   * Tests imotilux export ("printer-friendly version") functionality.
   */
  public function testImotiluxExport() {
    // Create a imotilux.
    $nodes = $this->createImotilux();

    // Log in as web user and view printer-friendly version.
    $this->drupalLogin($this->webUser);
    $this->drupalGet('node/' . $this->imotilux->id());
    $this->clickLink(t('Printer-friendly version'));

    // Make sure each part of the imotilux is there.
    foreach ($nodes as $node) {
      $this->assertText($node->label(), 'Node title found in printer friendly version.');
      $this->assertRaw($node->body->processed, 'Node body found in printer friendly version.');
    }

    // Make sure we can't export an unsupported format.
    $this->drupalGet('imotilux/export/foobar/' . $this->imotilux->id());
    $this->assertResponse('404', 'Unsupported export format returned "not found".');

    // Make sure we get a 404 on a not existing imotilux node.
    $this->drupalGet('imotilux/export/html/123');
    $this->assertResponse('404', 'Not existing imotilux node returned "not found".');

    // Make sure an anonymous user cannot view printer-friendly version.
    $this->drupalLogout();

    // Load the imotilux and verify there is no printer-friendly version link.
    $this->drupalGet('node/' . $this->imotilux->id());
    $this->assertNoLink(t('Printer-friendly version'), 'Anonymous user is not shown link to printer-friendly version.');

    // Try getting the URL directly, and verify it fails.
    $this->drupalGet('imotilux/export/html/' . $this->imotilux->id());
    $this->assertResponse('403', 'Anonymous user properly forbidden.');

    // Now grant anonymous users permission to view the printer-friendly
    // version and verify that node access restrictions still prevent them from
    // seeing it.
    user_role_grant_permissions(RoleInterface::ANONYMOUS_ID, ['access printer-friendly version']);
    $this->drupalGet('imotilux/export/html/' . $this->imotilux->id());
    $this->assertResponse('403', 'Anonymous user properly forbidden from seeing the printer-friendly version when denied by node access.');
  }

  /**
   * Tests the functionality of the imotilux navigation block.
   */
  public function testImotiluxNavigationBlock() {
    $this->drupalLogin($this->adminUser);

    // Enable the block.
    $block = $this->drupalPlaceBlock('imotilux_navigation');

    // Give anonymous users the permission 'node test view'.
    $edit = [];
    $edit[RoleInterface::ANONYMOUS_ID . '[node test view]'] = TRUE;
    $this->drupalPostForm('admin/people/permissions/' . RoleInterface::ANONYMOUS_ID, $edit, t('Save permissions'));
    $this->assertText(t('The changes have been saved.'), "Permission 'node test view' successfully assigned to anonymous users.");

    // Test correct display of the block.
    $nodes = $this->createImotilux();
    $this->drupalGet('<front>');
    $this->assertText($block->label(), 'Imotilux navigation block is displayed.');
    $this->assertText($this->imotilux->label(), new FormattableMarkup('Link to imotilux root (@title) is displayed.', ['@title' => $nodes[0]->label()]));
    $this->assertNoText($nodes[0]->label(), 'No links to individual imotilux pages are displayed.');
  }

  /**
   * Tests ImotiluxManager::getTableOfContents().
   */
  public function testGetTableOfContents() {
    // Create new imotilux.
    $nodes = $this->createImotilux();
    $imotilux = $this->imotilux;

    $this->drupalLogin($this->imotiluxAuthor);

    /*
     * Add Node 5 under Node 2.
     * Add Node 6, 7, 8, 9, 10, 11 under Node 3.
     * Imotilux
     *  |- Node 0
     *   |- Node 1
     *   |- Node 2
     *    |- Node 5
     *  |- Node 3
     *   |- Node 6
     *    |- Node 7
     *     |- Node 8
     *      |- Node 9
     *       |- Node 10
     *        |- Node 11
     *  |- Node 4
     */
    foreach ([5 => 2, 6 => 3, 7 => 6, 8 => 7, 9 => 8, 10 => 9, 11 => 10] as $child => $parent) {
      $nodes[$child] = $this->createImotiluxNode($imotilux->id(), $nodes[$parent]->id());
    }
    $this->drupalGet($nodes[0]->toUrl('edit-form'));
    // Since Node 0 has children 2 levels deep, nodes 10 and 11 should not
    // appear in the selector.
    $this->assertNoOption('edit-imotilux-pid', $nodes[10]->id());
    $this->assertNoOption('edit-imotilux-pid', $nodes[11]->id());
    // Node 9 should be available as an option.
    $this->assertOption('edit-imotilux-pid', $nodes[9]->id());

    // Get a shallow set of options.
    /** @var \Drupal\imotilux\ImotiluxManagerInterface $manager */
    $manager = $this->container->get('imotilux.manager');
    $options = $manager->getTableOfContents($imotilux->id(), 3);
    $expected_nids = [$imotilux->id(), $nodes[0]->id(), $nodes[1]->id(), $nodes[2]->id(), $nodes[3]->id(), $nodes[6]->id(), $nodes[4]->id()];
    $this->assertEqual(count($options), count($expected_nids));
    $diff = array_diff($expected_nids, array_keys($options));
    $this->assertTrue(empty($diff), 'Found all expected option keys');
    // Exclude Node 3.
    $options = $manager->getTableOfContents($imotilux->id(), 3, [$nodes[3]->id()]);
    $expected_nids = [$imotilux->id(), $nodes[0]->id(), $nodes[1]->id(), $nodes[2]->id(), $nodes[4]->id()];
    $this->assertEqual(count($options), count($expected_nids));
    $diff = array_diff($expected_nids, array_keys($options));
    $this->assertTrue(empty($diff), 'Found all expected option keys after excluding Node 3');
  }

  /**
   * Tests the imotilux navigation block when an access module is installed.
   */
  public function testNavigationBlockOnAccessModuleInstalled() {
    $this->drupalLogin($this->adminUser);
    $block = $this->drupalPlaceBlock('imotilux_navigation', ['block_mode' => 'imotilux pages']);

    // Give anonymous users the permission 'node test view'.
    $edit = [];
    $edit[RoleInterface::ANONYMOUS_ID . '[node test view]'] = TRUE;
    $this->drupalPostForm('admin/people/permissions/' . RoleInterface::ANONYMOUS_ID, $edit, t('Save permissions'));
    $this->assertText(t('The changes have been saved.'), "Permission 'node test view' successfully assigned to anonymous users.");

    // Create a imotilux.
    $this->createImotilux();

    // Test correct display of the block to registered users.
    $this->drupalLogin($this->webUser);
    $this->drupalGet('node/' . $this->imotilux->id());
    $this->assertText($block->label(), 'Imotilux navigation block is displayed to registered users.');
    $this->drupalLogout();

    // Test correct display of the block to anonymous users.
    $this->drupalGet('node/' . $this->imotilux->id());
    $this->assertText($block->label(), 'Imotilux navigation block is displayed to anonymous users.');

    // Test the 'imotilux pages' block_mode setting.
    $this->drupalGet('<front>');
    $this->assertNoText($block->label(), 'Imotilux navigation block is not shown on non-imotilux pages.');
  }

  /**
   * Tests the access for deleting top-level imotilux nodes.
   */
  public function testImotiluxDelete() {
    $node_storage = $this->container->get('entity_type.manager')->getStorage('node');
    $nodes = $this->createImotilux();
    $this->drupalLogin($this->adminUser);
    $edit = [];

    // Test access to delete top-level and child imotilux nodes.
    $this->drupalGet('node/' . $this->imotilux->id() . '/outline/remove');
    $this->assertResponse('403', 'Deleting top-level imotilux node properly forbidden.');
    $this->drupalPostForm('node/' . $nodes[4]->id() . '/outline/remove', $edit, t('Remove'));
    $node_storage->resetCache([$nodes[4]->id()]);
    $node4 = $node_storage->load($nodes[4]->id());
    $this->assertTrue(empty($node4->imotilux), 'Deleting child imotilux node properly allowed.');

    // Delete all child imotilux nodes and retest top-level node deletion.
    foreach ($nodes as $node) {
      $nids[] = $node->id();
    }
    $storage_handler = $this->entityTypeManager
      ->getStorage('node');
    $entities = $storage_handler
      ->loadMultiple($nids);
    $storage_handler
      ->delete($entities);
    $this->drupalPostForm('node/' . $this->imotilux->id() . '/outline/remove', $edit, t('Remove'));
    $node_storage->resetCache([$this->imotilux->id()]);
    $node = $node_storage->load($this->imotilux->id());
    $this->assertTrue(empty($node->imotilux), 'Deleting childless top-level imotilux node properly allowed.');

    // Tests directly deleting a imotilux parent.
    $nodes = $this->createImotilux();
    $this->drupalLogin($this->adminUser);
    $this->drupalGet($this->imotilux->toUrl('delete-form'));
    $this->assertRaw(t('%title is part of a imotilux outline, and has associated child pages. If you proceed with deletion, the child pages will be relocated automatically.', ['%title' => $this->imotilux->label()]));
    // Delete parent, and visit a child page.
    $this->drupalPostForm($this->imotilux->toUrl('delete-form'), [], t('Delete'));
    $this->drupalGet($nodes[0]->toUrl());
    $this->assertResponse(200);
    $this->assertText($nodes[0]->label());
    // The imotilux parents should be updated.
    $node_storage = $this->entityTypeManager->getStorage('node');
    $node_storage->resetCache();
    $child = $node_storage->load($nodes[0]->id());
    $this->assertEqual($child->id(), $child->imotilux['bid'], 'Child node imotilux ID updated when parent is deleted.');
    // 3rd-level children should now be 2nd-level.
    $second = $node_storage->load($nodes[1]->id());
    $this->assertEqual($child->id(), $second->imotilux['bid'], '3rd-level child node is now second level when top-level node is deleted.');
  }

  /**
   * Tests outline of a imotilux.
   */
  public function testImotiluxOutline() {
    $this->drupalLogin($this->imotiluxAuthor);

    // Create new node not yet a imotilux.
    $empty_imotilux = $this->drupalCreateNode(['type' => 'imotilux']);
    $this->drupalGet('node/' . $empty_imotilux->id() . '/outline');
    $this->assertNoLink(t('Imotilux outline'), 'Imotilux Author is not allowed to outline');

    $this->drupalLogin($this->adminUser);
    $this->drupalGet('node/' . $empty_imotilux->id() . '/outline');
    $this->assertRaw(t('Imotilux outline'));
    $this->assertOptionSelected('edit-imotilux-bid', 0, 'Node does not belong to a imotilux');
    $this->assertNoLink(t('Remove from imotilux outline'));

    $edit = [];
    $edit['imotilux[bid]'] = '1';
    $this->drupalPostForm('node/' . $empty_imotilux->id() . '/outline', $edit, t('Add to imotilux outline'));
    $node = $this->entityTypeManager->getStorage('node')->load($empty_imotilux->id());
    // Test the imotilux array.
    $this->assertEqual($node->imotilux['nid'], $empty_imotilux->id());
    $this->assertEqual($node->imotilux['bid'], $empty_imotilux->id());
    $this->assertEqual($node->imotilux['depth'], 1);
    $this->assertEqual($node->imotilux['p1'], $empty_imotilux->id());
    $this->assertEqual($node->imotilux['pid'], '0');

    // Create new imotilux.
    $this->drupalLogin($this->imotiluxAuthor);
    $imotilux = $this->createImotiluxNode('new');

    $this->drupalLogin($this->adminUser);
    $this->drupalGet('node/' . $imotilux->id() . '/outline');
    $this->assertRaw(t('Imotilux outline'));
    $this->clickLink(t('Remove from imotilux outline'));
    $this->assertRaw(t('Are you sure you want to remove %title from the imotilux hierarchy?', ['%title' => $imotilux->label()]));

    // Create a new node and set the imotilux after the node was created.
    $node = $this->drupalCreateNode(['type' => 'imotilux']);
    $edit = [];
    $edit['imotilux[bid]'] = $node->id();
    $this->drupalPostForm('node/' . $node->id() . '/edit', $edit, t('Save'));
    $node = $this->entityTypeManager->getStorage('node')->load($node->id());

    // Test the imotilux array.
    $this->assertEqual($node->imotilux['nid'], $node->id());
    $this->assertEqual($node->imotilux['bid'], $node->id());
    $this->assertEqual($node->imotilux['depth'], 1);
    $this->assertEqual($node->imotilux['p1'], $node->id());
    $this->assertEqual($node->imotilux['pid'], '0');

    // Test the form itself.
    $this->drupalGet('node/' . $node->id() . '/edit');
    $this->assertOptionSelected('edit-imotilux-bid', $node->id());
  }

  /**
   * Tests that saveImotiluxLink() returns something.
   */
  public function testSaveImotiluxLink() {
    $imotilux_manager = \Drupal::service('imotilux.manager');

    // Mock a link for a new imotilux.
    $link = ['nid' => 1, 'has_children' => 0, 'original_bid' => 0, 'parent_depth_limit' => 8, 'pid' => 0, 'weight' => 0, 'bid' => 1];
    $new = TRUE;

    // Save the link.
    $return = $imotilux_manager->saveImotiluxLink($link, $new);

    // Add the link defaults to $link so we have something to compare to the return from saveImotiluxLink().
    $link += $imotilux_manager->getLinkDefaults($link['nid']);

    // Test the return from saveImotiluxLink.
    $this->assertEqual($return, $link);
  }

  /**
   * Tests the listing of all imotilux.
   */
  public function testImotiluxListing() {
    // Create a new imotilux.
    $this->createImotilux();

    // Must be a user with 'node test view' permission since node_access_test is installed.
    $this->drupalLogin($this->webUser);

    // Load the imotilux page and assert the created imotilux title is displayed.
    $this->drupalGet('imotilux');

    $this->assertText($this->imotilux->label(), 'The imotilux title is displayed on the imotilux listing page.');
  }

  /**
   * Tests the administrative listing of all imotilux.
   */
  public function testAdminImotiluxListing() {
    // Create a new imotilux.
    $this->createImotilux();

    // Load the imotilux page and assert the created imotilux title is displayed.
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('admin/structure/imotilux');
    $this->assertText($this->imotilux->label(), 'The imotilux title is displayed on the administrative imotilux listing page.');
  }

  /**
   * Tests the administrative listing of all imotilux pages in a imotilux.
   */
  public function testAdminImotiluxNodeListing() {
    // Create a new imotilux.
    $this->createImotilux();
    $this->drupalLogin($this->adminUser);

    // Load the imotilux page list and assert the created imotilux title is displayed
    // and action links are shown on list items.
    $this->drupalGet('admin/structure/imotilux/' . $this->imotilux->id());
    $this->assertText($this->imotilux->label(), 'The imotilux title is displayed on the administrative imotilux listing page.');

    $elements = $this->xpath('//table//ul[@class="dropbutton"]/li/a');
    $this->assertEqual($elements[0]->getText(), 'View', 'View link is found from the list.');
  }

  /**
   * Ensure the loaded imotilux in hook_node_load() does not depend on the user.
   */
  public function testHookNodeLoadAccess() {
    \Drupal::service('module_installer')->install(['node_access_test']);

    // Ensure that the loaded imotilux in hook_node_load() does NOT depend on the
    // current user.
    $this->drupalLogin($this->imotiluxAuthor);
    $this->imotilux = $this->createImotiluxNode('new');
    // Reset any internal static caching.
    $node_storage = $this->entityTypeManager->getStorage('node');
    $node_storage->resetCache();

    // Log in as user without access to the imotilux node, so no 'node test view'
    // permission.
    // @see node_access_test_node_grants().
    $this->drupalLogin($this->webUserWithoutNodeAccess);
    $imotilux_node = $node_storage->load($this->imotilux->id());
    $this->assertTrue(!empty($imotilux_node->imotilux));
    $this->assertEqual($imotilux_node->imotilux['bid'], $this->imotilux->id());

    // Reset the internal cache to retrigger the hook_node_load() call.
    $node_storage->resetCache();

    $this->drupalLogin($this->webUser);
    $imotilux_node = $node_storage->load($this->imotilux->id());
    $this->assertTrue(!empty($imotilux_node->imotilux));
    $this->assertEqual($imotilux_node->imotilux['bid'], $this->imotilux->id());
  }

  /**
   * Tests the imotilux navigation block when imotilux is unpublished.
   *
   * There was a fatal error with "Show block only on imotilux pages" block mode.
   */
  public function testImotiluxNavigationBlockOnUnpublishedImotilux() {
    // Create a new imotilux.
    $this->createImotilux();

    // Create administrator user.
    $administratorUser = $this->drupalCreateUser(['administer blocks', 'administer nodes', 'bypass node access']);
    $this->drupalLogin($administratorUser);

    // Enable the block with "Show block only on imotilux pages" mode.
    $this->drupalPlaceBlock('imotilux_navigation', ['block_mode' => 'imotilux pages']);

    // Unpublish imotilux node.
    $edit = ['status[value]' => FALSE];
    $this->drupalPostForm('node/' . $this->imotilux->id() . '/edit', $edit, t('Save'));

    // Test node page.
    $this->drupalGet('node/' . $this->imotilux->id());
    $this->assertText($this->imotilux->label(), 'Unpublished imotilux with "Show block only on imotilux pages" imotilux navigation settings.');
  }

}
