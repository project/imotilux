<?php

namespace Drupal\Tests\imotilux\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Create a imotilux, add pages, and test imotilux interface.
 *
 * @group imotilux
 */
class ImotiluxBreadcrumbTest extends BrowserTestBase {

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = ['imotilux', 'block', 'imotilux_breadcrumb_test'];

  /**
   * A imotilux node.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $imotilux;

  /**
   * A user with permission to create and edit imotilux.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $imotiluxAuthor;

  /**
   * A user without the 'node test view' permission.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $webUserWithoutNodeAccess;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\user\Entity\User|false $adminUser
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->drupalPlaceBlock('system_breadcrumb_block');
    $this->drupalPlaceBlock('page_title_block');

    // Create users.
    $this->imotiluxAuthor = $this->drupalCreateUser(['create new imotilux', 'create imotilux content', 'edit own imotilux content', 'add content to imotilux']);
    $this->adminUser = $this->drupalCreateUser(['create new imotilux', 'create imotilux content', 'edit any imotilux content', 'delete any imotilux content', 'add content to imotilux', 'administer blocks', 'administer permissions', 'administer imotilux outlines', 'administer content types', 'administer site configuration']);
  }

  /**
   * Creates a new imotilux with a page hierarchy.
   *
   * @return \Drupal\node\NodeInterface[]
   *   The created imotilux nodes.
   */
  protected function createBreadcrumbImotilux() {
    // Create new imotilux.
    $this->drupalLogin($this->imotiluxAuthor);

    $this->imotilux = $this->createImotiluxNode('new');
    $imotilux = $this->imotilux;

    /*
     * Add page hierarchy to imotilux.
     * Imotilux
     *  |- Node 0
     *   |- Node 1
     *   |- Node 2
     *    |- Node 3
     *     |- Node 4
     *      |- Node 5
     *  |- Node 6
     */
    $nodes = [];
    $nodes[0] = $this->createImotiluxNode($imotilux->id());
    $nodes[1] = $this->createImotiluxNode($imotilux->id(), $nodes[0]->id());
    $nodes[2] = $this->createImotiluxNode($imotilux->id(), $nodes[0]->id());
    $nodes[3] = $this->createImotiluxNode($imotilux->id(), $nodes[2]->id());
    $nodes[4] = $this->createImotiluxNode($imotilux->id(), $nodes[3]->id());
    $nodes[5] = $this->createImotiluxNode($imotilux->id(), $nodes[4]->id());
    $nodes[6] = $this->createImotiluxNode($imotilux->id());

    $this->drupalLogout();

    return $nodes;
  }

  /**
   * Creates a imotilux node.
   *
   * @param int|string $imotilux_nid
   *   A imotilux node ID or set to 'new' to create a new imotilux.
   * @param int|null $parent
   *   (optional) Parent imotilux reference ID. Defaults to NULL.
   *
   * @return \Drupal\node\NodeInterface
   *   The created node.
   */
  protected function createImotiluxNode($imotilux_nid, $parent = NULL) {
    // $number does not use drupal_static as it should not be reset since it
    // uniquely identifies each call to createImotiluxNode(). It is used to ensure
    // that when sorted nodes stay in same order.
    static $number = 0;

    $edit = [];
    $edit['title[0][value]'] = str_pad($number, 2, '0', STR_PAD_LEFT) . ' - SimpleTest test node ' . $this->randomMachineName(10);
    $edit['body[0][value]'] = 'SimpleTest test body ' . $this->randomMachineName(32) . ' ' . $this->randomMachineName(32);
    $edit['imotilux[bid]'] = $imotilux_nid;

    if ($parent !== NULL) {
      $this->drupalGet('node/add/imotilux');
      $this->submitForm($edit, t('Change imotilux (update list of parents)'));

      $edit['imotilux[pid]'] = $parent;
      $this->submitForm($edit, t('Save'));
      // Make sure the parent was flagged as having children.
      $parent_node = $this->entityTypeManager->getStorage('node')->loadUnchanged($parent);
      $this->assertFalse(empty($parent_node->imotilux['has_children']), 'Parent node is marked as having children');
    }
    else {
      $this->drupalGet('node/add/imotilux');
      $this->submitForm($edit, t('Save'));
    }

    // Check to make sure the imotilux node was created.
    $node = $this->drupalGetNodeByTitle($edit['title[0][value]']);
    $this->assertNotNull(($node === FALSE ? NULL : $node), 'Imotilux node found in database.');
    $number++;

    return $node;
  }

  /**
   * Test that the breadcrumb is updated when imotilux content changes.
   */
  public function testBreadcrumbTitleUpdates() {
    // Create a new imotilux.
    $nodes = $this->createBreadcrumbImotilux();
    $imotilux = $this->imotilux;

    $this->drupalLogin($this->imotiluxAuthor);

    $this->drupalGet($nodes[4]->toUrl());
    // Fetch each node title in the current breadcrumb.
    $links = $this->xpath('//nav[@class="breadcrumb"]/ol/li/a');
    $got_breadcrumb = [];
    foreach ($links as $link) {
      $got_breadcrumb[] = $link->getText();
    }
    // Home link and four parent imotilux nodes should be in the breadcrumb.
    $this->assertEquals(5, count($got_breadcrumb));
    $this->assertEquals($nodes[3]->getTitle(), end($got_breadcrumb));
    $edit = [
      'title[0][value]' => 'Updated node5 title',
    ];
    $this->drupalGet($nodes[3]->toUrl('edit-form'));
    $this->submitForm($edit, 'Save');
    $this->drupalGet($nodes[4]->toUrl());
    // Fetch each node title in the current breadcrumb.
    $links = $this->xpath('//nav[@class="breadcrumb"]/ol/li/a');
    $got_breadcrumb = [];
    foreach ($links as $link) {
      $got_breadcrumb[] = $link->getText();
    }
    $this->assertEquals(5, count($got_breadcrumb));
    $this->assertEquals($edit['title[0][value]'], end($got_breadcrumb));
  }

  /**
   * Test that the breadcrumb is updated when imotilux access changes.
   */
  public function testBreadcrumbAccessUpdates() {
    // Create a new imotilux.
    $nodes = $this->createBreadcrumbImotilux();
    $this->drupalLogin($this->imotiluxAuthor);
    $edit = [
      'title[0][value]' => "you can't see me",
    ];
    $this->drupalGet($nodes[3]->toUrl('edit-form'));
    $this->submitForm($edit, 'Save');
    $this->drupalGet($nodes[4]->toUrl());
    $links = $this->xpath('//nav[@class="breadcrumb"]/ol/li/a');
    $got_breadcrumb = [];
    foreach ($links as $link) {
      $got_breadcrumb[] = $link->getText();
    }
    $this->assertEquals(5, count($got_breadcrumb));
    $this->assertEquals($edit['title[0][value]'], end($got_breadcrumb));
    $config = $this->container->get('config.factory')->getEditable('imotilux_breadcrumb_test.settings');
    $config->set('hide', TRUE)->save();
    $this->drupalGet($nodes[4]->toUrl());
    $links = $this->xpath('//nav[@class="breadcrumb"]/ol/li/a');
    $got_breadcrumb = [];
    foreach ($links as $link) {
      $got_breadcrumb[] = $link->getText();
    }
    $this->assertEquals(4, count($got_breadcrumb));
    $this->assertEquals($nodes[2]->getTitle(), end($got_breadcrumb));
    $this->drupalGet($nodes[3]->toUrl());
    $this->assertSession()->statusCodeEquals(403);
  }

}
