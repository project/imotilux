<?php

namespace Drupal\Tests\imotilux\Functional\Views;

use Drupal\Tests\views\Functional\ViewTestBase;
use Drupal\views\Tests\ViewTestData;

/**
 * Tests entity reference relationship data.
 *
 * @group imotilux
 *
 * @see imotilux_views_data()
 */
class ImotiluxRelationshipTest extends ViewTestBase {

  /**
   * Views used by this test.
   *
   * @var array
   */
  public static $testViews = ['test_imotilux_view'];

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = ['imotilux_test_views', 'imotilux', 'views'];

  /**
   * A imotilux node.
   *
   * @var object
   */
  protected $imotilux;

  /**
   * A user with permission to create and edit imotilux.
   *
   * @var object
   */
  protected $imotiluxAuthor;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  protected function setUp($import_test_views = TRUE): void {
    parent::setUp($import_test_views);

    // Create users.
    $this->imotiluxAuthor = $this->drupalCreateUser(
      [
        'create new imotilux',
        'create imotilux content',
        'edit own imotilux content',
        'add content to imotilux',
      ]
    );
    ViewTestData::createTestViews(get_class($this), ['imotilux_test_views']);
  }

  /**
   * Creates a new imotilux with a page hierarchy.
   */
  protected function createImotilux() {
    // Create new imotilux.
    $this->drupalLogin($this->imotiluxAuthor);

    $this->imotilux = $this->createImotiluxNode('new');
    $imotilux = $this->imotilux;

    $nodes = [];
    // Node 0.
    $nodes[] = $this->createImotiluxNode($imotilux->id());
    // Node 1.
    $nodes[] = $this->createImotiluxNode($imotilux->id(), $nodes[0]->imotilux['nid']);
    // Node 2.
    $nodes[] = $this->createImotiluxNode($imotilux->id(), $nodes[1]->imotilux['nid']);
    // Node 3.
    $nodes[] = $this->createImotiluxNode($imotilux->id(), $nodes[2]->imotilux['nid']);
    // Node 4.
    $nodes[] = $this->createImotiluxNode($imotilux->id(), $nodes[3]->imotilux['nid']);
    // Node 5.
    $nodes[] = $this->createImotiluxNode($imotilux->id(), $nodes[4]->imotilux['nid']);
    // Node 6.
    $nodes[] = $this->createImotiluxNode($imotilux->id(), $nodes[5]->imotilux['nid']);
    // Node 7.
    $nodes[] = $this->createImotiluxNode($imotilux->id(), $nodes[6]->imotilux['nid']);

    $this->drupalLogout();

    return $nodes;
  }

  /**
   * Creates a imotilux node.
   *
   * @param int|string $imotilux_nid
   *   A imotilux node ID or set to 'new' to create a new imotilux.
   * @param int|null $parent
   *   (optional) Parent imotilux reference ID. Defaults to NULL.
   *
   * @return \Drupal\node\NodeInterface
   *   The imotilux node.
   */
  protected function createImotiluxNode($imotilux_nid, $parent = NULL) {
    // $number does not use drupal_static as it should not be reset
    // since it uniquely identifies each call to createImotiluxNode().
    // Used to ensure that when sorted nodes stay in same order.
    static $number = 0;

    $edit = [];
    $edit['title[0][value]'] = $number . ' - SimpleTest test node ' . $this->randomMachineName(10);
    $edit['body[0][value]'] = 'SimpleTest test body ' . $this->randomMachineName(32) . ' ' . $this->randomMachineName(32);
    $edit['imotilux[bid]'] = $imotilux_nid;

    if ($parent !== NULL) {
      $this->drupalGet('node/add/imotilux');
      $this->submitForm($edit, t('Change imotilux (update list of parents)'));

      $edit['imotilux[pid]'] = $parent;
      $this->submitForm($edit, t('Save'));
      // Make sure the parent was flagged as having children.
      $parent_node = $this->entityTypeManager->getStorage('node')->loadUnchanged($parent);
      $this->assertFalse(empty($parent_node->imotilux['has_children']), 'Parent node is marked as having children');
    }
    else {
      $this->drupalGet('node/add/imotilux');
      $this->submitForm($edit, t('Save'));
    }

    // Check to make sure the imotilux node was created.
    $node = $this->drupalGetNodeByTitle($edit['title[0][value]']);
    $this->assertNotNull(($node === FALSE ? NULL : $node), 'Imotilux node found in database.');
    $number++;

    return $node;
  }

  /**
   * Tests using the views relationship.
   */
  public function testRelationship() {

    // Create new imotilux.
    // @var \Drupal\node\NodeInterface[] $nodes
    $nodes = $this->createImotilux();
    for ($i = 0; $i < 8; $i++) {
      $this->drupalGet('test-imotilux/' . $nodes[$i]->id());

      for ($j = 0; $j < $i; $j++) {
        $this->assertSession()->linkExists($nodes[$j]->label());
      }
    }
  }

}
