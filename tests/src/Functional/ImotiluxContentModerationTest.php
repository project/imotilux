<?php

namespace Drupal\Tests\imotilux\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\content_moderation\Traits\ContentModerationTestTrait;

/**
 * Tests Imotilux and Content Moderation integration.
 *
 * @group imotilux
 */
class ImotiluxContentModerationTest extends BrowserTestBase {

  use ImotiluxTestTrait;
  use ContentModerationTestTrait;

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = ['imotilux', 'block', 'imotilux_test', 'content_moderation'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->drupalPlaceBlock('system_breadcrumb_block');
    $this->drupalPlaceBlock('page_title_block');

    $workflow = $this->createEditorialWorkflow();
    $workflow->getTypePlugin()->addEntityTypeAndBundle('node', 'imotilux');
    $workflow->save();

    // We need a user with additional content moderation permissions.
    $this->imotiluxAuthor = $this->drupalCreateUser(['create new imotilux', 'create imotilux content', 'edit own imotilux content', 'add content to imotilux', 'access printer-friendly version', 'view any unpublished content', 'use editorial transition create_new_draft', 'use editorial transition publish']);
  }

  /**
   * Tests that imotilux drafts can not modify the imotilux outline.
   */
  public function testImotiluxWithPendingRevisions() {
    // Create two imotilux.
    $imotilux_1_nodes = $this->createImotilux(['moderation_state[0][state]' => 'published']);
    $imotilux_1 = $this->imotilux;

    $this->createImotilux(['moderation_state[0][state]' => 'published']);
    $imotilux_2 = $this->imotilux;

    $this->drupalLogin($this->imotiluxAuthor);

    // Check that imotilux pages display along with the correct outlines.
    $this->imotilux = $imotilux_1;
    $this->checkImotiluxNode($imotilux_1, [$imotilux_1_nodes[0], $imotilux_1_nodes[3], $imotilux_1_nodes[4]], FALSE, FALSE, $imotilux_1_nodes[0], []);
    $this->checkImotiluxNode($imotilux_1_nodes[0], [$imotilux_1_nodes[1], $imotilux_1_nodes[2]], $imotilux_1, $imotilux_1, $imotilux_1_nodes[1], [$imotilux_1]);

    // Create a new imotilux page without actually attaching it to a imotilux and create
    // a draft.
    $edit = [
      'title[0][value]' => $this->randomString(),
      'moderation_state[0][state]' => 'published',
    ];
    $this->drupalGet('node/add/imotilux');
    $this->submitForm($edit, t('Save'));
    $node = $this->drupalGetNodeByTitle($edit['title[0][value]']);
    $this->assertTrue($node);

    $edit = [
      'moderation_state[0][state]' => 'draft',
    ];
    $this->drupalGet('node/' . $node->id() . '/edit');
    $this->submitForm($edit, t('Save'));
    $this->assertSession()->pageTextNotContains('You can only change the imotilux outline for the published version of this content.');

    // Create a imotilux draft with no changes, then publish it.
    $edit = [
      'moderation_state[0][state]' => 'draft',
    ];
    $this->drupalGet('node/' . $imotilux_1->id() . '/edit');
    $this->submitForm($edit, t('Save'));
    $this->assertSession()->pageTextNotContains('You can only change the imotilux outline for the published version of this content.');
    $edit = [
      'moderation_state[0][state]' => 'published',
    ];
    $this->drupalGet('node/' . $imotilux_1->id() . '/edit');
    $this->submitForm($edit, t('Save'));

    // Try to move Node 2 to a different parent.
    $edit = [
      'imotilux[pid]' => $imotilux_1_nodes[3]->id(),
      'moderation_state[0][state]' => 'draft',
    ];
    $this->drupalGet('node/' . $imotilux_1_nodes[1]->id() . '/edit');
    $this->submitForm($edit, t('Save'));

    $this->assertSession()->pageTextContains('You can only change the imotilux outline for the published version of this content.');

    // Check that the imotilux outline did not change.
    $this->imotilux = $imotilux_1;
    $this->checkImotiluxNode($imotilux_1, [$imotilux_1_nodes[0], $imotilux_1_nodes[3], $imotilux_1_nodes[4]], FALSE, FALSE, $imotilux_1_nodes[0], []);
    $this->checkImotiluxNode($imotilux_1_nodes[0], [$imotilux_1_nodes[1], $imotilux_1_nodes[2]], $imotilux_1, $imotilux_1, $imotilux_1_nodes[1], [$imotilux_1]);

    // Try to move Node 2 to a different imotilux.
    $edit = [
      'imotilux[bid]' => $imotilux_2->id(),
      'moderation_state[0][state]' => 'draft',
    ];
    $this->drupalGet('node/' . $imotilux_1_nodes[1]->id() . '/edit');
    $this->submitForm($edit, t('Save'));

    $this->assertSession()->pageTextContains('You can only change the imotilux outline for the published version of this content.');

    // Check that the imotilux outline did not change.
    $this->imotilux = $imotilux_1;
    $this->checkImotiluxNode($imotilux_1, [$imotilux_1_nodes[0], $imotilux_1_nodes[3], $imotilux_1_nodes[4]], FALSE, FALSE, $imotilux_1_nodes[0], []);
    $this->checkImotiluxNode($imotilux_1_nodes[0], [$imotilux_1_nodes[1], $imotilux_1_nodes[2]], $imotilux_1, $imotilux_1, $imotilux_1_nodes[1], [$imotilux_1]);

    // Try to change the weight of Node 2.
    $edit = [
      'imotilux[weight]' => 2,
      'moderation_state[0][state]' => 'draft',
    ];
    $this->drupalGet('node/' . $imotilux_1_nodes[1]->id() . '/edit');
    $this->submitForm($edit, t('Save'));

    $this->assertSession()->pageTextContains('You can only change the imotilux outline for the published version of this content.');

    // Check that the imotilux outline did not change.
    $this->imotilux = $imotilux_1;
    $this->checkImotiluxNode($imotilux_1, [$imotilux_1_nodes[0], $imotilux_1_nodes[3], $imotilux_1_nodes[4]], FALSE, FALSE, $imotilux_1_nodes[0], []);
    $this->checkImotiluxNode($imotilux_1_nodes[0], [$imotilux_1_nodes[1], $imotilux_1_nodes[2]], $imotilux_1, $imotilux_1, $imotilux_1_nodes[1], [$imotilux_1]);

    // Save a new draft revision for the node without any changes and check that
    // the error message is not displayed.
    $edit = [
      'moderation_state[0][state]' => 'draft',
    ];
    $this->drupalGet('node/' . $imotilux_1_nodes[1]->id() . '/edit');
    $this->submitForm($edit, t('Save'));

    $this->assertSession()->pageTextNotContains('You can only change the imotilux outline for the published version of this content.');
  }

}
