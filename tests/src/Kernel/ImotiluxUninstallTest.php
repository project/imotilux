<?php

namespace Drupal\Tests\imotilux\Kernel;

use Drupal\node\Entity\Node;
use Drupal\node\Entity\NodeType;
use Drupal\KernelTests\KernelTestBase;

/**
 * Tests that the Imotilux module cannot be uninstalled if imotilux exist.
 *
 * @group imotilux
 */
class ImotiluxUninstallTest extends KernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['system', 'user', 'field', 'filter', 'text', 'node', 'imotilux'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('user');
    $this->installEntitySchema('node');
    $this->installSchema('imotilux', ['imotilux']);
    $this->installSchema('node', ['node_access']);
    $this->installConfig(['node', 'imotilux', 'field']);
    // For uninstall to work.
    $this->installSchema('user', ['users_data']);
  }

  /**
   * Tests the imotilux_system_info_alter() method.
   */
  public function testImotiluxUninstall() {
    // No nodes exist.
    $validation_reasons = \Drupal::service('module_installer')->validateUninstall(['imotilux']);
    $this->assertEquals([], $validation_reasons, 'The imotilux module is not required.');

    $content_type = NodeType::create([
      'type' => $this->randomMachineName(),
      'name' => $this->randomString(),
    ]);
    $content_type->save();
    $imotilux_config = $this->config('imotilux.settings');
    $allowed_types = $imotilux_config->get('allowed_types');
    $allowed_types[] = $content_type->id();
    $imotilux_config->set('allowed_types', $allowed_types)->save();

    $node = Node::create(['title' => $this->randomString(), 'type' => $content_type->id()]);
    $node->imotilux['bid'] = 'new';
    $node->save();

    // One node in a imotilux but not of type imotilux.
    $validation_reasons = \Drupal::service('module_installer')->validateUninstall(['imotilux']);
    $this->assertEquals(['To uninstall Imotilux, delete all content that is part of a imotilux'], $validation_reasons['imotilux']);

    $imotilux_node = Node::create(['title' => $this->randomString(), 'type' => 'imotilux']);
    $imotilux_node->imotilux['bid'] = FALSE;
    $imotilux_node->save();

    // Two nodes, one in a imotilux but not of type imotilux and one imotilux node (which is
    // not in a imotilux).
    $validation_reasons = \Drupal::service('module_installer')->validateUninstall(['imotilux']);
    $this->assertEquals(['To uninstall Imotilux, delete all content that is part of a imotilux'], $validation_reasons['imotilux']);

    $node->delete();
    // One node of type imotilux but not actually part of a imotilux.
    $validation_reasons = \Drupal::service('module_installer')->validateUninstall(['imotilux']);
    $this->assertEquals(['To uninstall Imotilux, delete all content that has the Imotilux content type'], $validation_reasons['imotilux']);

    $imotilux_node->delete();
    // No nodes exist therefore the imotilux module is not required.
    $module_data = \Drupal::service('extension.list.module')->reset()->getList();
    $this->assertFalse(isset($module_data['imotilux']->info['required']), 'The imotilux module is not required.');

    $node = Node::create(['title' => $this->randomString(), 'type' => $content_type->id()]);
    $node->save();
    // One node exists but is not part of a imotilux therefore the imotilux module is
    // not required.
    $validation_reasons = \Drupal::service('module_installer')->validateUninstall(['imotilux']);
    $this->assertEquals([], $validation_reasons, 'The imotilux module is not required.');

    // Uninstall the Imotilux module and check the node type is deleted.
    \Drupal::service('module_installer')->uninstall(['imotilux']);
    $this->assertNull(NodeType::load('imotilux'), "The imotilux node type does not exist.");
  }

}
