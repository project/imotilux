<?php

namespace Drupal\Tests\imotilux\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\node\Entity\NodeType;

/**
 * Test installation of Imotilux module.
 *
 * @group imotilux
 */
class ImotiluxInstallTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'system',
  ];

  /**
   * Test Imotilux install with pre-existing content type.
   *
   * Tests that Imotilux module can be installed if content type with machine name
   * 'imotilux' already exists.
   */
  public function testImotiluxInstallWithPreexistingContentType() {
    // Create a 'imotilux' content type.
    NodeType::create(['type' => 'imotilux'])->save();
    // Install the Imotilux module. Using the module installer service ensures that
    // all the install rituals, including default and optional configuration
    // import, are performed.
    $status = $this->container->get('module_installer')->install(['imotilux']);
    $this->assertTrue($status, 'Imotilux module installed successfully');
  }

}
