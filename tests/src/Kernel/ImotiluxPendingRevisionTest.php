<?php

namespace Drupal\Tests\imotilux\Kernel;

use Drupal\node\Entity\Node;
use Drupal\node\Entity\NodeType;
use Drupal\KernelTests\KernelTestBase;

/**
 * Tests that the Imotilux module handles pending revisions correctly.
 *
 * @group imotilux
 */
class ImotiluxPendingRevisionTest extends KernelTestBase {

  /**
   * The mocked entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $entityTypeManager;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['system', 'user', 'field', 'filter', 'text', 'node', 'imotilux'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('user');
    $this->installEntitySchema('node');
    $this->installSchema('imotilux', ['imotilux']);
    $this->installSchema('node', ['node_access']);
    $this->installConfig(['node', 'imotilux', 'field']);
  }

  /**
   * Tests pending revision handling for imotilux.
   */
  public function testImotiluxWithPendingRevisions() {
    $content_type = NodeType::create([
      'type' => $this->randomMachineName(),
      'name' => $this->randomString(),
    ]);
    $content_type->save();
    $imotilux_config = $this->config('imotilux.settings');
    $allowed_types = $imotilux_config->get('allowed_types');
    $allowed_types[] = $content_type->id();
    $imotilux_config->set('allowed_types', $allowed_types)->save();

    // Create two top-level imotilux a child.
    $imotilux_1 = Node::create(['title' => $this->randomString(), 'type' => $content_type->id()]);
    $imotilux_1->imotilux['bid'] = 'new';
    $imotilux_1->save();
    $imotilux_1_child = Node::create(['title' => $this->randomString(), 'type' => $content_type->id()]);
    $imotilux_1_child->imotilux['bid'] = $imotilux_1->id();
    $imotilux_1_child->imotilux['pid'] = $imotilux_1->id();
    $imotilux_1_child->save();

    $imotilux_2 = Node::create(['title' => $this->randomString(), 'type' => $content_type->id()]);
    $imotilux_2->imotilux['bid'] = 'new';
    $imotilux_2->save();

    $child = Node::create(['title' => $this->randomString(), 'type' => $content_type->id()]);
    $child->imotilux['bid'] = $imotilux_1->id();
    $child->imotilux['pid'] = $imotilux_1->id();
    $child->save();

    // Try to move the child to a different imotilux while saving it as a pending
    // revision.
    /** @var \Drupal\imotilux\ImotiluxManagerInterface $imotilux_manager */
    $imotilux_manager = $this->container->get('imotilux.manager');

    // Check that the API doesn't allow us to change the imotilux outline for
    // pending revisions.
    $child->imotilux['bid'] = $imotilux_2->id();
    $child->setNewRevision(TRUE);
    $child->isDefaultRevision(FALSE);

    $this->assertFalse($imotilux_manager->updateOutline($child), 'A pending revision can not change the imotilux outline.');

    // Check that the API doesn't allow us to change the imotilux parent for
    // pending revisions.
    $child = $this->entityTypeManager->getStorage('node')->loadUnchanged($child->id());
    $child->imotilux['pid'] = $imotilux_1_child->id();
    $child->setNewRevision(TRUE);
    $child->isDefaultRevision(FALSE);

    $this->assertFalse($imotilux_manager->updateOutline($child), 'A pending revision can not change the imotilux outline.');

    // Check that the API doesn't allow us to change the imotilux weight for
    // pending revisions.
    $child = $this->entityTypeManager->getStorage('node')->loadUnchanged($child->id());
    $child->imotilux['weight'] = 2;
    $child->setNewRevision(TRUE);
    $child->isDefaultRevision(FALSE);

    $this->assertFalse($imotilux_manager->updateOutline($child), 'A pending revision can not change the imotilux outline.');
  }

}
