<?php

namespace Drupal\Tests\imotilux\Unit;

use Drupal\Tests\AssertHelperTrait;
use Drupal\Tests\UnitTestCase;

/**
 * @coversDefaultClass \Drupal\imotilux\ImotiluxUninstallValidator
 * @group imotilux
 */
class ImotiluxUninstallValidatorTest extends UnitTestCase {

  use AssertHelperTrait;

  /**
   * @var \Drupal\imotilux\ImotiluxUninstallValidator|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $imotiluxUninstallValidator;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->imotiluxUninstallValidator = $this->createMock('Drupal\imotilux\ImotiluxUninstallValidator');
    $this->imotiluxUninstallValidator->setStringTranslation($this->getStringTranslationStub());
  }

  /**
   * @covers ::validate
   */
  public function testValidateNotImotilux() {
    $this->imotiluxUninstallValidator->expects($this->never())
      ->method('hasImotiluxOutlines');
    $this->imotiluxUninstallValidator->expects($this->never())
      ->method('hasImotiluxNodes');

    $module = 'not_imotilux';
    $expected = [];
    $reasons = $this->imotiluxUninstallValidator->validate($module);
    $this->assertSame($expected, $this->castSafeStrings($reasons));
  }

  /**
   * @covers ::validate
   */
  public function testValidateEntityQueryWithoutResults() {
    $this->imotiluxUninstallValidator->expects($this->once())
      ->method('hasImotiluxOutlines')
      ->willReturn(FALSE);
    $this->imotiluxUninstallValidator->expects($this->once())
      ->method('hasImotiluxNodes')
      ->willReturn(FALSE);

    $module = 'imotilux';
    $expected = [];
    $reasons = $this->imotiluxUninstallValidator->validate($module);
    $this->assertSame($expected, $this->castSafeStrings($reasons));
  }

  /**
   * @covers ::validate
   */
  public function testValidateEntityQueryWithResults() {
    $this->imotiluxUninstallValidator->expects($this->once())
      ->method('hasImotiluxOutlines')
      ->willReturn(FALSE);
    $this->imotiluxUninstallValidator->expects($this->once())
      ->method('hasImotiluxNodes')
      ->willReturn(TRUE);

    $module = 'imotilux';
    $expected = ['To uninstall Imotilux, delete all content that has the Imotilux content type'];
    $reasons = $this->imotiluxUninstallValidator->validate($module);
    $this->assertSame($expected, $this->castSafeStrings($reasons));
  }

  /**
   * @covers ::validate
   */
  public function testValidateOutlineStorage() {
    $this->imotiluxUninstallValidator->expects($this->once())
      ->method('hasImotiluxOutlines')
      ->willReturn(TRUE);
    $this->imotiluxUninstallValidator->expects($this->never())
      ->method('hasImotiluxNodes');

    $module = 'imotilux';
    $expected = ['To uninstall Imotilux, delete all content that is part of a imotilux'];
    $reasons = $this->imotiluxUninstallValidator->validate($module);
    $this->assertSame($expected, $this->castSafeStrings($reasons));
  }

}
