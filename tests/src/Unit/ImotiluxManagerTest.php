<?php

namespace Drupal\Tests\imotilux\Unit;

use Drupal\imotilux\ImotiluxManager;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Tests\UnitTestCase;

/**
 * @coversDefaultClass \Drupal\imotilux\ImotiluxManager
 * @group imotilux
 */
class ImotiluxManagerTest extends UnitTestCase {

  /**
   * The mocked entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $entityTypeManager;

  /**
   * The mocked config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $configFactory;

  /**
   * The mocked translation manager.
   *
   * @var \Drupal\Core\StringTranslation\TranslationInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $translation;

  /**
   * The mocked renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $renderer;

  /**
   * The tested imotilux manager.
   *
   * @var \Drupal\imotilux\ImotiluxManager
   */
  protected $imotiluxManager;

  /**
   * Imotilux outline storage.
   *
   * @var \Drupal\imotilux\ImotiluxOutlineStorageInterface
   */
  protected $imotiluxOutlineStorage;

  /**
   * @var \Drupal\Core\Language\LanguageManagerInterface $languageManager
   */
  protected $languageManager;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    $this->entityTypeManager = $this->createMock(EntityTypeManagerInterface::class);
    $this->translation = $this->getStringTranslationStub();
    $this->configFactory = $this->getConfigFactoryStub([]);
    $this->imotiluxOutlineStorage = $this->getMockBuilder('Drupal\imotilux\ImotiluxOutlineStorageInterface');
    $this->renderer = $this->getMockBuilder('\Drupal\Core\Render\RendererInterface');
    $this->languageManager = $this->getMockBuilder('\Drupal\Core\Language\LanguageManagerInterface');
    $this->imotiluxManager = new ImotiluxManager($this->entityTypeManager, $this->translation, $this->configFactory, $this->imotiluxOutlineStorage, $this->renderer, $this->languageManager);
  }

  /**
   * Tests the getImotiluxParents() method.
   *
   * @dataProvider providerTestGetImotiluxParents
   */
  public function testGetImotiluxParents($imotilux, $parent, $expected) {
    $this->assertEquals($expected, $this->imotiluxManager->getImotiluxParents($imotilux, $parent));
  }

  /**
   * Provides test data for testGetImotiluxParents.
   *
   * @return array
   *   The test data.
   */
  public function providerTestGetImotiluxParents() {
    $empty = [
      'p1' => 0,
      'p2' => 0,
      'p3' => 0,
      'p4' => 0,
      'p5' => 0,
      'p6' => 0,
      'p7' => 0,
      'p8' => 0,
      'p9' => 0,
    ];
    return [
      // Provides a imotilux without an existing parent.
      [
        ['pid' => 0, 'nid' => 12],
        [],
        ['depth' => 1, 'p1' => 12] + $empty,
      ],
      // Provides a imotilux with an existing parent.
      [
        ['pid' => 11, 'nid' => 12],
        ['nid' => 11, 'depth' => 1, 'p1' => 11],
        ['depth' => 2, 'p1' => 11, 'p2' => 12] + $empty,
      ],
      // Provides a imotilux with two existing parents.
      [
        ['pid' => 11, 'nid' => 12],
        ['nid' => 11, 'depth' => 2, 'p1' => 10, 'p2' => 11],
        ['depth' => 3, 'p1' => 10, 'p2' => 11, 'p3' => 12] + $empty,
      ],
    ];
  }

}
