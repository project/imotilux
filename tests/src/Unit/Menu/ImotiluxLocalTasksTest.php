<?php

namespace Drupal\Tests\imotilux\Unit\Menu;

use Drupal\Tests\Core\Menu\LocalTaskIntegrationTestBase;

/**
 * Tests existence of imotilux local tasks.
 *
 * @group imotilux
 */
class ImotiluxLocalTasksTest extends LocalTaskIntegrationTestBase {

  protected function setUp(): void {
    $this->directoryList = [
      'imotilux' => 'core/modules/imotilux',
      'node' => 'core/modules/node',
    ];
    parent::setUp();
  }

  /**
   * Tests local task existence.
   *
   * @dataProvider getImotiluxAdminRoutes
   */
  public function testImotiluxAdminLocalTasks($route) {

    $this->assertLocalTasks($route, [
      0 => ['imotilux.admin', 'imotilux.settings'],
    ]);
  }

  /**
   * Provides a list of routes to test.
   */
  public function getImotiluxAdminRoutes() {
    return [
      ['imotilux.admin'],
      ['imotilux.settings'],
    ];
  }

  /**
   * Tests local task existence.
   *
   * @dataProvider getImotiluxNodeRoutes
   */
  public function testImotiluxNodeLocalTasks($route) {
    $this->assertLocalTasks($route, [
      0 => ['entity.node.imotilux_outline_form', 'entity.node.canonical', 'entity.node.edit_form', 'entity.node.delete_form', 'entity.node.version_history'],
    ]);
  }

  /**
   * Provides a list of routes to test.
   */
  public function getImotiluxNodeRoutes() {
    return [
      ['entity.node.canonical'],
      ['entity.node.imotilux_outline_form'],
    ];
  }

}
